import { useState } from "react";

export default function useToken() {
  const getToken = () => {
    const tokenString = sessionStorage.getItem("token");
    const userToken = JSON.parse(tokenString);

    return { token: userToken?.token || "", userId: userToken?.userId || "" };
  };

  const [token, setToken] = useState(getToken());

  const saveToken = (userToken: { token: string; userId: string }) => {
    sessionStorage.setItem("token", JSON.stringify(userToken));
    setToken({ token: userToken.token, userId: userToken.userId });
  };

  const clearToken = () => {
    sessionStorage.clear();
    saveToken({ token: "", userId: "" });
  };

  return {
    token,
    setToken: saveToken,
    clearToken,
  };
}
