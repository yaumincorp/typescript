// hooks/query/useWord.js

import { useQuery } from "react-query";
import { getDictionary } from "../../api/word";

type Word = {
  name: string;
};

export const useWord = () => {
  const queryKey = ["dictionary"];

  return useQuery<Array<string>, Error>("dictionary", getDictionary);

  /* return {
    ...useQuery(queryKey, () => getDictionary.get()),
    invalidate: queryCache.invalidateQueries(queryKey),
  }; */
};
