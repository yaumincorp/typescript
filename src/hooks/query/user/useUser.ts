// hooks/query/useUser.js

import { useQuery } from "react-query";
import { getUser, getUsers } from "../../../api/user";
import { User } from "../../../interfaces";

/* type User = {
  name: string;
}; */

export function useUsers(limit: number, skip: number) {
  const queryKey = ["dictionary"];

  return useQuery(["users", [limit, skip]], () => getUsers(limit, skip), {
    retry: 2,
    keepPreviousData: true,
  });
}

/* L'appel à la fonction getUser n'est autorisé que si userId a une longueur, donc !== "" */
export function useUser(userId: string) {
  console.log("USER ID", userId);
  return useQuery(["user", userId], async () => getUser(userId), {
    enabled: !!userId.length,
  });
}
