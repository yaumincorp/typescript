import { useReducer, useState } from "react";
import { useNavigate } from "react-router-dom";
import { createUser } from "../../../api/user";
import { validateInput } from "../../../helpers/formHelpers";
import { AppProps } from "../../../interfaces";
import Input from "../../shared/Input/input";

const initialState = {
  username: { value: "", touched: false, hasError: true, error: "" },
  firstname: { value: "", touched: false, hasError: true, error: "" },
  lastname: { value: "", touched: false, hasError: true, error: "" },
  age: { value: 0, touched: false, hasError: true, error: "" },
  password: { value: "", touched: false, hasError: true, error: "" },
  isFormValid: false,
};

const reducer = (state: any, action: any) => {
  switch (action.type) {
    case "updateForm":
      const { name, value, hasError, error, touched, isFormValid } =
        action.payload;

      console.log("payload", action.payload);

      console.log("name reducer", name);
      console.log("value reducer", value);

      return {
        ...state,
        [name]: { ...state[name], value, hasError, error, touched },
        isFormValid,
      };
    case "reset":
      return initialState;
    default:
      return state;
  }
};

const SignupPage = ({ routeName }: AppProps) => {
  const [displayPassword, setDisplayPassword] = useState<boolean>(false);
  const [state, dispatch] = useReducer(reducer, initialState);
  const [showError, setShowError] = useState(false);

  let navigate = useNavigate();

  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    let isFormValid = true;

    for (const name in state) {
      const item = state[name];

      const { value } = item;

      const { hasError, error } = validateInput(name, value);

      if (hasError) {
        isFormValid = false;
      }

      if (name) {
        dispatch({
          type: "updateForm",
          payload: {
            name,
            value,
            hasError,
            error,
            touched: true,
            isFormValid,
          },
        });
      }
    }

    if (!isFormValid) {
      setShowError(true);
    } else {
      //Logic to submit the form to backend
      await createUser({
        age: state.age.value,
        firstname: state.firstname.value,
        lastname: state.lastname.value,
        username: state.username.value,
        password: state.password.value,
      });
      navigate(`/`);
    }
    // Hide the error message after 5 seconds
    setTimeout(() => {
      setShowError(false);
    }, 5000);
  };

  const handleChange = (
    name: string,
    value: string,
    dispatch: any,
    formState: any
  ) => {
    let isFormValid = true;
    const { error, hasError } = validateInput(name, value);

    for (const key in formState) {
      const item = formState[key];

      // Check if the current field has error

      if (key === name && hasError) {
        isFormValid = false;

        break;
      } else if (key !== name && item.hasError) {
        // Check if any other field has error

        isFormValid = false;

        break;
      }
    }

    console.log("form valid", isFormValid);
    console.log("value", value);
    console.log("name", name);

    dispatch({
      type: "updateForm",
      payload: { name, value, hasError, error, touched: false, isFormValid },
    });
  };

  const onFocusOut = (
    name: string,
    value: string,
    dispatch: any,
    formState: any
  ) => {
    const { hasError, error } = validateInput(name, value);

    let isFormValid = true;

    for (const key in formState) {
      const item = formState[key];

      if (key === name && hasError) {
        isFormValid = false;

        break;
      } else if (key !== name && item.hasError) {
        isFormValid = false;

        break;
      }
    }
    dispatch({
      type: "updateForm",
      payload: { name, value, hasError, error, touched: true, isFormValid },
    });
  };

  console.log(state);

  return (
    <div className="md:container md:mx-auto">
      <h1 className="text-3xl font-bold underline">{routeName}</h1>
      <div className="my-4">
        <form onSubmit={onSubmit} className="flex flex-col items-center">
          <Input
            id="firstname"
            type="text"
            name="firstname"
            placeholder="Firstname"
            dataTestid="input-firstname"
            icon={""}
            onChange={(e: any) => {
              handleChange("firstname", e.target.value, dispatch, state);
            }}
            onBlur={(e: any) => {
              onFocusOut("firstname", e.target.value, dispatch, state);
            }}
            value={state.firstname.value}
          />
          {state.firstname.touched && state.firstname.hasError && (
            <div className="error">{state.firstname.error}</div>
          )}
          <Input
            id="lastname"
            type="text"
            name="lastname"
            placeholder="Lastname"
            dataTestid="input-lastname"
            icon={""}
            onChange={(e: any) => {
              handleChange("lastname", e.target.value, dispatch, state);
            }}
            onBlur={(e: any) => {
              onFocusOut("lastname", e.target.value, dispatch, state);
            }}
            value={state.lastname.value}
          />
          {state.lastname.touched && state.lastname.hasError && (
            <div className="error">{state.lastname.error}</div>
          )}
          <Input
            id="username"
            type="text"
            name="username"
            placeholder="Username"
            dataTestid="input-username"
            icon={""}
            onChange={(e: any) => {
              handleChange("username", e.target.value, dispatch, state);
            }}
            onBlur={(e: any) => {
              onFocusOut("username", e.target.value, dispatch, state);
            }}
            value={state.username.value}
          />
          {state.username.touched && state.username.hasError && (
            <div className="error">{state.username.error}</div>
          )}
          <Input
            id="age"
            type="number"
            name="age"
            placeholder="Age"
            dataTestid="input-age"
            icon={""}
            onChange={(e: any) => {
              handleChange("age", e.target.value, dispatch, state);
            }}
            onBlur={(e: any) => {
              onFocusOut("age", e.target.value, dispatch, state);
            }}
            value={state.age.value}
          />
          {state.age.touched && state.age.hasError && (
            <div className="error">{state.age.error}</div>
          )}
          <Input
            id="password"
            type={displayPassword ? "text" : "password"}
            name="password"
            placeholder="Password"
            dataTestid="input-password"
            icon={""}
            onChange={(e: any) => {
              handleChange("password", e.target.value, dispatch, state);
            }}
            onBlur={(e: any) => {
              onFocusOut("password", e.target.value, dispatch, state);
            }}
            value={state.password.value}
            isLastIcon={true}
            lastIconFunction={setDisplayPassword}
            paramLastIconFunction={!displayPassword}
          />
          {state.password.touched && state.password.hasError && (
            <div className="error">{state.password.error}</div>
          )}
          <div className="col-span-2">
            {showError && !state.isFormValid && (
              <div className="form_error">
                Please fill all the fields correctly
              </div>
            )}
            <button className="button" type="submit">
              Sign Up
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default SignupPage;
