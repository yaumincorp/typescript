import { useContext, useEffect, useReducer, useState } from "react";
import { TestContext } from "../../shared/Context/TestContextProvider";

const initialState = {
  isConfirmed: false,
};

const reducer = (state = initialState, action: any) => {
  switch (action.type) {
    case "SUCCESS":
      return {
        ...state,
        isConfirmed: true,
      };
    default:
      throw Error("unknown action");
  }
};

const TestPage = ({}) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  //toggle state
  const [toggleButton, setToggleButton] = useState(false);

  //pure array
  const [todos, setTodos] = useState([]);

  //array with properties

  const [todosProperties, setTodosProperties] = useState({
    items: [],
    dataIsLoaded: false,
  });

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/todos?id=1&id=2")
      .then((response) => response.json())
      .then((json) => {
        console.log(json);
        setTodos(json);
      });
    fetch("https://jsonplaceholder.typicode.com/todos?id=3&id=4")
      .then((response) => response.json())
      .then((json) => {
        console.log(json);
        setTodosProperties({ items: json, dataIsLoaded: true });
      });
  }, []);

  //Exemple de fetch directement dans le composant

  const { name } = useContext(TestContext);

  /*
    Exercice railways :

    Instruction en français

    On veut construire une voie ferrée longue de n kilomètres, sachant qu'on dispose de rails de 1, 2 et 3 km.
    Ecrire une fonction qui, étant donné n, renvoie le nombre de façons différentes de réaliser ce projet.
    On considérera que deux rails de même longueur sont équivalents : cela signifie que les solutions a-b-c (1-1-3) 
    et b-a-c (1-1-3) ne comptent que pour une solution ; le rail a et le rail b faisant chacun 1km, 
    ils sont donc “interchangeables”.
    Par contre a-d-c (1-2-3) et a-c-d (1-3-2) comptent pour 2 solutions distinctes.

    Exemple :

    Pour n = 4, les solutions étant :

    =.=.=.= (1-1-1-1) V
    =.==.= (1-2-1)
    =.=.== (1-1-2) V
    ==.=.= (2-1-1) V
    ==.== (2-2)
    ===.= (3-1) V
    =.=== (1-3)

    La fonction renvoie donc 7. 
*/

  /*

 const attackGroup = (playerCardInGround, enemyCardInGround) => {
  let allAttacks = [];

  //Si une carte présente, la faire attaquer l'ennemi
  const cardToAttack = playerCardInGround.map(element => element.instanceId);

  //Si l'ennemi pose des cartes avec gardes, les détruire d'abord
  const enemyCardsWithGuard = enemyCardInGround.filter(element => element.abilities.includes('G'));

  console.error("enemyCardsWithGuard", enemyCardsWithGuard)

  enemyCardsWithGuard.forEach((enemyCard)=>{
      playerCardInGround.forEach((playerCard) => {
          console.error("playerCard", playerCard)
          console.error("enemyCard", enemyCard)

          //Si l'ennemi guard a une défense faible, faire attaquer la carte qui a l'attaque exacte
          if(playerCard.attack === enemyCard.defense){
              allAttacks.push('ATTACK ' + playerCard.instanceId + ' ' + enemyCard.instanceId)
          }
          //if(playerCard.attack > enemyCard.defense){
          allAttacks.push('ATTACK ' + playerCard.instanceId + ' ' + enemyCard.instanceId)
          //}
      })
  });

  console.error("card to attack in attack group function", cardToAttack)

  if(cardToAttack){
      cardToAttack.forEach((item) => {
          console.error("item in attack group function",item)
          allAttacks.push('ATTACK ' + item + ' ' + -1);
      });
  }

  return allAttacks;
}

const summonCreatureOrUseObject = (item, playerCards, enemyCards, fullMana=false) => {

  console.error("item",item);
  console.error("fullMana",fullMana);

  let summonedCards = [];
  let summonedCardsCost = 0;

  const highestAttackPlayerCreature = playerCards.sort((a, b) => b.attack - a.attack)[0];
  console.error("highestAttackPlayerCreature",highestAttackPlayerCreature);

  const enemiesCreatureWithGuard = enemyCards.filter(enemy => enemy.abilities.includes("G"));
  console.error("enemiesCreatureWithGuard",enemiesCreatureWithGuard);

  const enemyCreatureWithGuard = enemiesCreatureWithGuard.sort((a, b) => b.defense - a.defense)[0];
  console.error("enemyCreatureWithGuard",enemyCreatureWithGuard);

  if(fullMana){
      const highestAttack = item.sort((a, b) => b.attack - a.attack)[0];
      console.error("highestAttack",highestAttack)
      if(highestAttackPlayerCreature && highestAttack.cardType === 1){
          summonedCards.push('USE ' + highestAttack.instanceId + ' ' + highestAttackPlayerCreature.instanceId);
      }
      else if(highestAttack.cardType === 0){
          summonedCards.push('SUMMON ' + highestAttack.instanceId)
      }
      summonedCardsCost += highestAttack.cost;
  }
  else{
      console.error("coucou");
      //Booster ses créatures
      if(item.cardType === 1){
          summonedCards.push('USE ' + item.instanceId + ' ' + highestAttackPlayerCreature.instanceId);
      }
      //Diminuer les créatures adverses qui ont guard en attribut
      else if(item.cardType === 2 && enemyCreatureWithGuard){
          summonedCards.push('USE ' + item.instanceId + ' ' + enemyCreatureWithGuard.instanceId);
      }
      //Invocation classique
      else if(item.cardType === 0){
          summonedCards.push('SUMMON ' + item.instanceId);
      }
      summonedCardsCost += item.cost;
  }

  return {summonedCards, summonedCardsCost};
};

const findCardToPutOnBattleGround = (mana, hand, playerCardInGround, enemyCardInGround) => {

  const fullManaCards = hand.filter(item => item.cost === mana);
  const manaEightCards = hand.filter(item => item.cost === 8);
  const manaSevenCards = hand.filter(item => item.cost === 7);
  const manaSixCards = hand.filter(item => item.cost === 6);
  const manaFiveCards = hand.filter(item => item.cost === 5);
  const manaFourCards = hand.filter(item => item.cost === 4);
  const manaThreeCards = hand.filter(item => item.cost === 3);
  const manaTwoCards = hand.filter(item => item.cost === 2);
  const manaOneCard = hand.filter(item => item.cost === 1);

  let summonCards = [];

  //Pour un tour donné, essayer de dépenser le plus de mana possible
  let manaSpent = 0;
  //Récupérer le mana total du tour et soustraire le mana total avec le mana des cartes que l'on veut poser
  while(mana-manaSpent > 0){
      console.error("JE TOURNE", mana-manaSpent)
      console.error("JE TOURNE mana", mana)
      console.error("JE TOURNE manaSpent", manaSpent)
      //Pose de cartes
      if(fullManaCards.length) {
          //Deck aggro, carte avec la plus grosse attaque
          //Deck aggro, des cartes avec des capacités spéciales peuvent être choisies (charge, dommage aux opposants)

          const {summonedCards, summonedCardsCost} = summonCreatureOrUseObject(fullManaCards, playerCardInGround, enemyCardInGround, true);
          summonCards.push(summonedCards);
          manaSpent += summonedCardsCost;
      }
      else if(manaEightCards.length && mana-manaSpent >= 8){
          manaEightCards.forEach(item => {
              const {summonedCards, summonedCardsCost} =  summonCreatureOrUseObject(item, playerCardInGround, enemyCardInGround)
              summonCards.push(summonedCards);
              manaSpent += summonedCardsCost;
          });
      }
      else if(manaSevenCards.length && mana-manaSpent >= 7){
          manaSevenCards.forEach(item => {
              const {summonedCards, summonedCardsCost} =  summonCreatureOrUseObject(item, playerCardInGround, enemyCardInGround)
              summonCards.push(summonedCards);
              manaSpent += summonedCardsCost;
          });
      }
      else if(manaSixCards.length && mana-manaSpent >= 6){
          manaSixCards.forEach(item => {
              const {summonedCards, summonedCardsCost} =  summonCreatureOrUseObject(item, playerCardInGround, enemyCardInGround)
              summonCards.push(summonedCards);
              manaSpent += summonedCardsCost;
          });
      }
      else if(manaFiveCards.length && mana-manaSpent >= 5){
          manaFiveCards.forEach(item => {
              const {summonedCards, summonedCardsCost} =  summonCreatureOrUseObject(item, playerCardInGround, enemyCardInGround)
              summonCards.push(summonedCards);
              manaSpent += summonedCardsCost;
          });
      }
      else if(manaFourCards.length && mana-manaSpent >= 4){
          manaFourCards.forEach(item => {
              const {summonedCards, summonedCardsCost} =  summonCreatureOrUseObject(item, playerCardInGround, enemyCardInGround)
              summonCards.push(summonedCards);
              manaSpent += summonedCardsCost;
          });
      }
      else if(manaThreeCards.length && mana-manaSpent >= 3){
          manaThreeCards.forEach(item => {
              const {summonedCards, summonedCardsCost} =  summonCreatureOrUseObject(item, playerCardInGround, enemyCardInGround)
              summonCards.push(summonedCards);
              manaSpent += summonedCardsCost;
          });
      }
      else if(manaTwoCards.length && mana-manaSpent >= 2){
          manaTwoCards.forEach(item => {
              const {summonedCards, summonedCardsCost} =  summonCreatureOrUseObject(item, playerCardInGround, enemyCardInGround)
              summonCards.push(summonedCards);
              manaSpent += summonedCardsCost;
          });
      }
      else if(manaOneCard.length && mana-manaSpent >= 1){
          manaOneCard.forEach(item => {
              const {summonedCards, summonedCardsCost} =  summonCreatureOrUseObject(item, playerCardInGround, enemyCardInGround)
              summonCards.push(summonedCards);
              manaSpent += summonedCardsCost;
          });
      }

      //DECK aggro
      //Par la suite, comparer selon le nombre de cartes et leurs attaques, quelle est la pose la plus aggressive

  }
  console.error("summonCards", summonCards)
  
  return summonCards;
}

const playerTurn = (playerAttacks, playerPut) => {
  let messageToLog = [];
  messageToLog = [...messageToLog, playerAttacks.join(";")];
  messageToLog = [...messageToLog, playerPut.join(";")];

  return messageToLog;
};

// game loop
while (true) {

  let mana = 0;

  let hand = [];
  let playerCardInGround = [];
  let enemyCardInGround = [];

  for (let i = 0; i < 2; i++) {
      var inputs = readline().split(' ');
      const playerHealth = parseInt(inputs[0]);
      const playerMana = parseInt(inputs[1]);
      const playerDeck = parseInt(inputs[2]);
      const playerRune = parseInt(inputs[3]);
      const playerDraw = parseInt(inputs[4]);

      mana = playerMana;
  }
  var inputs = readline().split(' ');
  const opponentHand = parseInt(inputs[0]);
  const opponentActions = parseInt(inputs[1]);

  for (let i = 0; i < opponentActions; i++) {
      const cardNumberAndAction = readline();
  }
  const cardCount = parseInt(readline());

  for (let i = 0; i < cardCount; i++) {
      var inputs = readline().split(' ');
      const cardNumber = parseInt(inputs[0]);
      const instanceId = parseInt(inputs[1]);
      const location = parseInt(inputs[2]);
      const cardType = parseInt(inputs[3]);
      const cost = parseInt(inputs[4]);
      const attack = parseInt(inputs[5]);
      const defense = parseInt(inputs[6]);
      const abilities = inputs[7];
      const myHealthChange = parseInt(inputs[8]);
      const opponentHealthChange = parseInt(inputs[9]);
      const cardDraw = parseInt(inputs[10]);

      const cardCharacteristics = {
          cost: cost,
          cardType: cardType, 
          instanceId: instanceId, 
          attack: attack, 
          defense: defense, 
          location: location,
          abilities: abilities,
          opponentHealthChange: opponentHealthChange
      };

      if(location === 0){
          hand.push(cardCharacteristics)
      }
      else if(location === 1){
          playerCardInGround.push(cardCharacteristics)
      }
      else if(location === -1){
          enemyCardInGround.push(cardCharacteristics)
      }
  }

  // Write an action using console.log()
  // To debug: console.error('Debug messages...');
  
  //Draft (Deck aggro)

  //Une fonction pour trouver la meilleure carte à poser chaque tour
  //Carte qui a un coût moindre, carte qui a une meilleure attaque ou une meilleure défense
  //que les autres cartes de la main

  //Game
      //mana = 12, ou 13 s'il a son bonus 
      //(bonus = deuxième joueur perds son bonus de 1 mana maximum dès lors qu'il dépense 
      //toute sa mana disponible en un tour)

      if(mana === 0){
          //DRAFT - créer un deck agressif
          //DECK agressif : prendre les cartes à faibles coût et avec la plus grosse attaque
          //Deck aggro, des cartes avec des capacités spéciales peuvent être choisies (charge, dommage aux opposants)
          let smallest = hand[0];

          console.error("smallest", smallest);

          for(let i=1; i<hand.length; i++){
              const findCharge = hand[i].abilities.includes('C') ? hand[i] : null;
              
              //compare a weapon and a creature
              if((hand[i].cardType === 0 && smallest.cardType === 2) && smallest.cost === 0){
                  smallest = hand[i];
              }

              else if(hand[i].cost < smallest.cost || (hand[i].cost === smallest.cost && hand[i].attack > smallest.attack)){
                  smallest = hand[i];   
              }
              else if(findCharge && findCharge.cost < smallest.cost){
                  smallest = findCharge;
              }
          }
          const nb = hand.findIndex(element => element === smallest);

          console.log('PICK ' + nb);
      }
      else if(mana === 1){
          //Carte de 1 mana
          const element = hand.find(item => item.cost === mana);
          if(element) {
              console.log('SUMMON ' + element.instanceId);
          }
          else console.log('PASS')
      }
      else if(mana === 2){

          //Si une ou plusieurs cartes présentes, les faire attaquer
          const playerAttacks = attackGroup(playerCardInGround, enemyCardInGround);
          
          //Carte de 1 mana, 1 mana *2 ou 2 manas
          //Pose de cartes
          const playerPut = findCardToPutOnBattleGround(mana, hand, playerCardInGround, enemyCardInGround);
          
          const logToSend = playerTurn(playerAttacks, playerPut);

          console.error("logToSend", logToSend)
          console.error("JE PASSE ICI, ce sera le mouvement global du tour", logToSend.join(';'));
          
          //if !messageToLog.length, on envoie pass sinon on envoie messageToLog
          if(logToSend.length){
              console.log(logToSend.join(';'))
          }
          else {
              console.log('PASS')
          }
      }
      else if(mana === 3){
          //Carte de 1 mana, 1 mana * 2, 1 mana *3, 2 manas + 1 mana ou 3 manas
          //Si une ou plusieurs cartes présentes, les faire attaquer
          const playerAttacks = attackGroup(playerCardInGround, enemyCardInGround);

          //Pose de cartes
          const playerPut = findCardToPutOnBattleGround(mana, hand, playerCardInGround, enemyCardInGround);

          const logToSend = playerTurn(playerAttacks, playerPut);

          console.error("logToSend", logToSend)

          if(logToSend.length){
              console.log(logToSend.join(';'))
          }
          else {
              console.log('PASS')
          }
      }
      else if(mana === 4){
          //Carte de 1 mana, 1 mana * 2, 1 mana *4, 2 manas * 2; 1 mana + 3 manas, 4 manas

          //Calculer l'attaque totale des cartes à deux de mana et à un de mana
          const attackOneCard = manaOneCard.reduce((acc, curr) =>{
              console.error("acc",acc)
              console.error("curr",curr)
              return acc + curr.attack;
          },0);
          const attackTwoCard = manaTwoCard.reduce((acc, curr) =>{
              return acc + curr.attack;
          },0);

          const attackFourCard = fullManaCard.attack;

          console.error("attackFourCard",attackFourCard)
          console.error("attackOneCard",attackOneCard)
          console.error("attackTwoCard",attackTwoCard)

          const o = { attackOneCard, attackTwoCard, attackFourCard};  
          
          const name = Object.entries(o).reduce((m, c) => m[1] > c[1] ? m: c)[0];
          
          console.error("name",name)
          const [nameCards] = Object.keys({attackTwoCard});

          if(name === nameCards){
              console.error("COUCOU", manaTwoCard)
              manaTwoCard.forEach(card => console.log('SUMMON ' + card.instanceId))
          }
          
          console.log('SUMMON ' + attackFourCard.instanceId); 
          

          //Si une ou plusieurs cartes présentes, les faire attaquer
          const playerAttacks = attackGroup(playerCardInGround, enemyCardInGround);

          //Pose de cartes
          const playerPut = findCardToPutOnBattleGround(mana, hand, playerCardInGround, enemyCardInGround);

          const logToSend = playerTurn(playerAttacks, playerPut);

          console.error("logToSend", logToSend)

          if(logToSend.length){
              console.log(logToSend.join(';'))
          }
          else {
              console.log('PASS')
          }

      }
      else if(mana === 5){
          //Si une ou plusieurs cartes présentes, les faire attaquer
          const playerAttacks = attackGroup(playerCardInGround, enemyCardInGround);

          //Pose de cartes
          const playerPut = findCardToPutOnBattleGround(mana, hand, playerCardInGround, enemyCardInGround);

          const logToSend = playerTurn(playerAttacks, playerPut);

          console.error("logToSend", logToSend)

          if(logToSend.length){
              console.log(logToSend.join(';'))
          }
          else {
              console.log('PASS')
          }
      }
      else if(mana === 6){
          //Si une ou plusieurs cartes présentes, les faire attaquer
          const playerAttacks = attackGroup(playerCardInGround, enemyCardInGround);

          //Pose de cartes
          const playerPut = findCardToPutOnBattleGround(mana, hand, playerCardInGround, enemyCardInGround);

          const logToSend = playerTurn(playerAttacks, playerPut);

          console.error("logToSend", logToSend)

          if(logToSend.length){
              console.log(logToSend.join(';'))
          }
          else {
              console.log('PASS')
          }
      }
      else if(mana === 7){
          //Si une ou plusieurs cartes présentes, les faire attaquer
          const playerAttacks = attackGroup(playerCardInGround, enemyCardInGround);

          //Pose de cartes
          const playerPut = findCardToPutOnBattleGround(mana, hand, playerCardInGround, enemyCardInGround);

          const logToSend = playerTurn(playerAttacks, playerPut);

          console.error("logToSend", logToSend)

          if(logToSend.length){
              console.log(logToSend.join(';'))
          }
          else {
              console.log('PASS')
          }
      }
      else if(mana === 8){
          //Si une ou plusieurs cartes présentes, les faire attaquer
          const playerAttacks = attackGroup(playerCardInGround, enemyCardInGround);

          //Pose de cartes
          const playerPut = findCardToPutOnBattleGround(mana, hand, playerCardInGround, enemyCardInGround);

          const logToSend = playerTurn(playerAttacks, playerPut);

          console.error("logToSend", logToSend)

          if(logToSend.length){
              console.log(logToSend.join(';'))
          }
          else {
              console.log('PASS')
          }
      }
      else if(mana === 9){
          //Si une ou plusieurs cartes présentes, les faire attaquer
          const playerAttacks = attackGroup(playerCardInGround, enemyCardInGround);

          //Pose de cartes
          const playerPut = findCardToPutOnBattleGround(mana, hand, playerCardInGround, enemyCardInGround);

          const logToSend = playerTurn(playerAttacks, playerPut);

          console.error("logToSend", logToSend)

          if(logToSend.length){
              console.log(logToSend.join(';'))
          }
          else {
              console.log('PASS')
          }
      }
      else if(mana === 10){
          //Si une ou plusieurs cartes présentes, les faire attaquer
          const playerAttacks = attackGroup(playerCardInGround, enemyCardInGround);

          //Pose de cartes
          const playerPut = findCardToPutOnBattleGround(mana, hand, playerCardInGround, enemyCardInGround);

          const logToSend = playerTurn(playerAttacks, playerPut);

          console.error("logToSend", logToSend)

          if(logToSend.length){
              console.log(logToSend.join(';'))
          }
          else {
              console.log('PASS')
          }
      }
      else if(mana === 11){
          //Si une ou plusieurs cartes présentes, les faire attaquer
          const playerAttacks = attackGroup(playerCardInGround, enemyCardInGround);

          //Pose de cartes
          const playerPut = findCardToPutOnBattleGround(mana, hand, playerCardInGround, enemyCardInGround);

          const logToSend = playerTurn(playerAttacks, playerPut);

          console.error("logToSend", logToSend)

          if(logToSend.length){
              console.log(logToSend.join(';'))
          }
          else {
              console.log('PASS')
          }
      }
      else if(mana === 12){
          //Si une ou plusieurs cartes présentes, les faire attaquer
          const playerAttacks = attackGroup(playerCardInGround, enemyCardInGround);

          //Pose de cartes
          const playerPut = findCardToPutOnBattleGround(mana, hand, playerCardInGround, enemyCardInGround);

          const logToSend = playerTurn(playerAttacks, playerPut);

          console.error("logToSend", logToSend)

          if(logToSend.length){
              console.log(logToSend.join(';'))
          }
          else {
              console.log('PASS')
          }
      }

  
  Actions possibles

      SUMMON id pour invoquer une créature
      ATTACK id1 id2 pour attaquer la créature id2 avec la créature id1.
      ATTACK id -1 pour attaquer l'adversaire avec la créature id.
      PASS pour passer son tour. 
  
  //console.log('PASS');
}*/

  /*
  The goal is to design a pyramid of given height. The building blocks are the letters A to Z.
  Entrée
  Line 1: The height H of the pyramid
  Sortie
  2×H-1 lines: The design of the pyramid, i.e. letters denoting height of the block (see the example) - no trailing spaces

  */

  /*
    A dice as been thrown and it is your job to find out the probability of a given number. The dice has not been loaded and is perfectly normal
    Entrée
    Line 1 : The integer n for which we must find the probability
    Line 2 : A string s containing all the dice's throwsas integers separated by spaces
    Sortie
    A unique line containing the probability of n in the sample s in percent (ex: 0,3 becomes 30 and 0,654 becomes 65)
    If n has not been thrown once you must output "No throws"
*/

  const railways = (railwayLength: number) => {
    const nbSolutions: any[] = [];

    const set_arr = [1, 2, 3];

    const nbLength = railwayLength;

    let finish: any[] = [];
    let working: any[] = [[]];

    while (working.length) {
      let next_work: any[] = [];

      for (const element of working) {
        for (const j of set_arr) {
          let subset = element.concat([j]);
          let sum = subset.reduce((acc: any, curr: any) => acc + curr, 0);
          if (sum <= nbLength) {
            (sum === nbLength ? finish : next_work).push(subset);
          }
        }
      }
      working = next_work;
    }
    //return finish;

    /* 
    for (let i = 0; i < Math.floor(nbLength / a); i++) {
      nbSolutionsA.push(a);
    }

    console.log("NB SOLUTIONS A", nbSolutionsA); */

    console.log("NB SOLUTIONS", finish);

    //Trouver les différents découpages

    /* const multipleTotal =
      Math.floor(nbLength / a) +
      Math.floor(nbLength / b) +
      Math.floor(nbLength / c);

    for (let i = 0; i < nbLength; i++) {
      console.log("A", i % a);
      console.log("B", i % b);
      console.log("C", i % c);

      nbSolutions.push([a]);
      nbSolutions.push([b]);
      nbSolutions.push([c]);

      if (i % a === 0) {
        nbSolutions.forEach((solution) => {
          const initialValue = 0;
          const sumWithInitial = solution.reduce(
            (previousValue: any, currentValue: any) =>
              previousValue + currentValue,
            initialValue
          );

          if (sumWithInitial < nbLength) {
            solution.push(a);
          }
        });
      }
      if (i % b === 0) {
        nbSolutions.forEach((solution) => {
          //console.log("solution B", solution + index);
          console.log("solution B", solution);
          const initialValue = 0;
          const sumWithInitial = solution.reduce(
            (previousValue: any, currentValue: any) =>
              previousValue + currentValue,
            initialValue
          );
          console.log("SUM", sumWithInitial);
          if (sumWithInitial < nbLength && sumWithInitial + b <= nbLength) {
            solution.push(b);
          }
        });
      }
      if (i % c === 0) {
        nbSolutions.forEach((solution) => {
          //console.log("solution C", solution + index);
          const initialValue = 0;
          const sumWithInitial = solution.reduce(
            (previousValue: any, currentValue: any) =>
              previousValue + currentValue,
            initialValue
          );
          console.log("SUM", sumWithInitial);
          if (sumWithInitial < nbLength && sumWithInitial + c <= nbLength) {
            solution.push(c);
          }
        });
      }
    } */

    const uniqueNbSolutions = new Set(nbSolutions.map((arr) => arr.join("-")));
    //.size;
    //console.log("NB SOLUTIONS", nbSolutions);
    //console.log(" UNIQUE NB SOLUTIONS", uniqueNbSolutions);
    return finish;
  };

  const result = railways(4);
  console.log("RESULT", result);

  return (
    <div>
      <button type="button" onClick={() => setToggleButton(!toggleButton)}>
        {toggleButton ? "TOGGLE" : "NOT TOGGLE"}
      </button>
      <div>
        <span>{name}</span>
        <div>
          {state.isConfirmed ? (
            <p>Confirmed!</p>
          ) : (
            <p>Waiting for confirmation...</p>
          )}
        </div>
        <button onClick={() => dispatch({ type: "SUCCESS" })}>Confirm</button>
      </div>
      {todosProperties.dataIsLoaded && (
        <div>
          {todosProperties.items.map((item, index) => (
            <span key={index}>{item.title}</span>
          ))}
        </div>
      )}
      {todos && (
        <div>
          {todos.map((item, index) => (
            <span key={index} data-testid="todo">
              {item.title}
            </span>
          ))}
        </div>
      )}
    </div>
  );
};

export default TestPage;
