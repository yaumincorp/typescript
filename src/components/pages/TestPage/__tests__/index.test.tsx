import TestPage from "..";

import {
  cleanup,
  fireEvent,
  getByText,
  render,
  screen,
} from "@testing-library/react";

import { QueryClient, QueryClientProvider } from "react-query";
import { TestContext } from "../../../shared/Context/TestContextProvider";

afterEach(cleanup);

describe.skip("Home page render test", () => {
  beforeEach(async () => {
    const mockTodos = [
      { completed: false, id: 1, title: "delectus aut autem", userId: 1 },
      {
        completed: false,
        id: 2,
        title: "quis ut nam facilis et officia qui",
        userId: 1,
      },
      {
        completed: false,
        id: 3,
        title: "mundis placet et sptiritus minima",
        userId: 1,
      },
      {
        completed: false,
        id: 4,
        title: "Rose rosam, et quid meliora",
        userId: 1,
      },
    ];
    jest.clearAllMocks();

    global.fetch = jest.fn(async () => ({
      ok: true,
      status: 200,
      json: async () => mockTodos.map((todo) => ({ ...todo })),
    })) as jest.Mock;
  });

  /**
   * A custom render to setup providers. Extends regular
   * render options with `providerProps` to allow injecting
   * different scenarios to test with.
   *
   * @see https://testing-library.com/docs/react-testing-library/setup#custom-render
   */
  const customRender = (ui: any, { providerProps, ...renderOptions }: any) => {
    return render(
      <TestContext.Provider {...providerProps}>{ui}</TestContext.Provider>,
      renderOptions
    );
  };

  it("Render without error", () => {
    render(<TestPage />);
  });

  it("Render without error, display toggle button", () => {
    render(<TestPage />);

    const toggleButton = screen.getByRole("button");
    expect(toggleButton.textContent).toBe("NOT TOGGLE");
    fireEvent.click(toggleButton);
    expect(toggleButton.textContent).toBe("TOGGLE");
  });

  it("Render without error, and get todo data 1 & 2", async () => {
    render(<TestPage />);

    const todos = await screen.findAllByTestId("todo");
    expect(todos[0]).toHaveTextContent("delectus aut autem");
    expect(todos[1]).toHaveTextContent("quis ut nam facilis et officia qui");
  });

  it("Render without error, and get todo data 3 & 4", async () => {
    render(<TestPage />);

    const todos = await screen.findAllByTestId("todo");
    expect(todos[2]).toHaveTextContent("mundis placet et sptiritus minima");
    expect(todos[3]).toHaveTextContent("Rose rosam, et quid meliora");
  });

  it("shows success message after confirm button is clicked", () => {
    render(<TestPage />);

    expect(screen.getByText(/waiting/i)).toBeInTheDocument();

    fireEvent.click(screen.getByText("Confirm"));

    expect(screen.getByText("Confirmed!")).toBeInTheDocument();
  });

  /**
   * To test a component that provides a context value, render a matching
   * consumer as the child
   */
  it("NameProvider composes full name from first, last", () => {
    const providerProps = {
      name: "Bastien",
    };
    customRender(<TestPage />, { providerProps });
  });
});
