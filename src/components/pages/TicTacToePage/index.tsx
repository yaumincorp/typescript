import { AppProps } from "../../../interfaces";
import BoxComponent from "./BoxComponent";
import useHook from "./hook";
import CasinoIcon from "@mui/icons-material/Casino";

const TicTacToePage = ({ routeName }: AppProps) => {
  const {
    chooseCross,
    chooseRound,
    playerSide,
    isVictory,
    isDefeat,
    startGame,
    allBoxes,
    isGameStarted,
    changeBox,
    firstPlayer,
    rollSide,
    resetAll,
  } = useHook();

  return (
    <>
      <h1 className="text-3xl font-bold underline">{routeName}</h1>
      <div className="items-center flex flex-col justify-center my-4">
        <div className="flex flex-col my-6">
          <div my-4>
            <CasinoIcon
              className="cursor-pointer text-emerald-600 hover:text-emerald-800 !text-3xl"
              onClick={rollSide}
            />
            {firstPlayer === "player" && (
              <>
                <h3 className="text-2xl">Choisissez votre camp:</h3>

                <button className="button p-2 m-2" onClick={chooseCross}>
                  X
                </button>
                <button className="button p-2 m-2" onClick={chooseRound}>
                  O
                </button>
              </>
            )}
          </div>
          <div>
            <span className="mx-4">First player: {firstPlayer}</span>
            <span className="mx-4">Camp: {playerSide}</span>
            {isVictory && <div className="bg-green-600">You have won</div>}
            {isDefeat && <div className="bg-red-600">You have lost</div>}
            {(isVictory || isDefeat) && (
              <button className="button" onClick={resetAll}>
                Restart
              </button>
            )}
            <button
              className="button"
              disabled={!playerSide}
              onClick={startGame}
            >
              Start game
            </button>
          </div>
        </div>
        <div className="grid grid-cols-3 tic-tac-toe">
          {allBoxes.map((boxItem, index) => {
            return (
              <BoxComponent
                key={index}
                isGameStarted={isGameStarted}
                changeBox={changeBox}
                boxItem={boxItem}
                playerSide={playerSide}
              />
            );
          })}
        </div>
      </div>
    </>
  );
};
export default TicTacToePage;
