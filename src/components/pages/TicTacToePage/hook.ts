import { useCallback, useEffect, useMemo, useState } from "react";
import ticTacToeHelper from "../../../helpers/ticTacToeHelper";
/* eslint import/no-anonymous-default-export: [2, {"allowAnonymousFunction": true}] */

const boxes = [
  {
    box: "tl",
    campChoice: "",
  },
  {
    box: "tc",
    campChoice: "",
  },
  {
    box: "tr",
    campChoice: "",
  },
  {
    box: "ml",
    campChoice: "",
  },
  {
    box: "mc",
    campChoice: "",
  },
  {
    box: "mr",
    campChoice: "",
  },
  {
    box: "bl",
    campChoice: "",
  },
  {
    box: "bc",
    campChoice: "",
  },
  {
    box: "br",
    campChoice: "",
  },
];

const victoryCases = [
  ["tl", "tc", "tr"],
  ["ml", "mc", "mr"],
  ["bl", "bc", "br"],
  ["tl", "mc", "br"],
  ["bl", "mc", "tr"],
  ["tl", "ml", "bl"],
  ["tc", "mc", "bc"],
  ["tr", "mr", "br"],
];

export default function () {
  const [playerSide, setPlayerSide] = useState<"cross" | "round" | null>(null);
  const [enemySide, setEnemySide] = useState<"cross" | "round" | null>(null);

  const [firstPlayer, setFirstPlayer] = useState<"player" | "ia" | null>(null);

  const [isPlayerTurn, setIsPlayerTurn] = useState(false);
  const [isIaTurn, setIsIaTurn] = useState(false);

  const [allBoxes, setAllBoxes] = useState(boxes);
  const [availableBoxes, setAvailableBoxes] = useState(boxes);
  const [iaBoxes, setIaBoxes] = useState([]);
  const [playerBoxes, setPlayerBoxes] = useState([]);

  const [isGameStarted, setIsGameStarted] = useState<boolean>(false);

  const [isVictory, setIsVictory] = useState<boolean>(false);
  const [isDefeat, setIsDefeat] = useState<boolean>(false);

  const startGame = () => {
    setIsGameStarted(true);
    if (firstPlayer === "ia") setIsIaTurn(true);
    else setIsPlayerTurn(true);
  };

  const chooseCross = () => {
    setPlayerSide("cross");
    setEnemySide("round");
  };

  const chooseRound = () => {
    setPlayerSide("round");
    setEnemySide("cross");
  };

  const rollSide = () => {
    const randomizeFirstPlayer: any = ["player", "ia"][
      Math.floor(Math.random() * ["player", "ia"].length)
    ];

    setFirstPlayer(randomizeFirstPlayer);
  };

  const resetAll = () => {
    const resetBoxes = [
      {
        box: "tl",
        campChoice: "",
      },
      {
        box: "tc",
        campChoice: "",
      },
      {
        box: "tr",
        campChoice: "",
      },
      {
        box: "ml",
        campChoice: "",
      },
      {
        box: "mc",
        campChoice: "",
      },
      {
        box: "mr",
        campChoice: "",
      },
      {
        box: "bl",
        campChoice: "",
      },
      {
        box: "bc",
        campChoice: "",
      },
      {
        box: "br",
        campChoice: "",
      },
    ];

    setIsGameStarted(false);
    console.log("BOXES", resetBoxes);
    setAvailableBoxes(resetBoxes);
    setAllBoxes(resetBoxes);
    setFirstPlayer(null);
    setEnemySide(null);
    setPlayerSide(null);
    setIsIaTurn(false);
    setIsPlayerTurn(false);
    setIsVictory(false);
    setIsDefeat(false);
  };

  useEffect(() => {
    if (firstPlayer === "ia") {
      const randomizeSide: any = ["round", "cross"][
        Math.floor(Math.random() * ["round", "cross"].length)
      ];
      setEnemySide(randomizeSide);
      const otherSide: any = ["round", "cross"].filter(
        (item) => item !== randomizeSide
      );
      setPlayerSide(otherSide.toString());
    }
  }, [firstPlayer]);

  const checkVictory = useCallback(
    (gameBoxes: any) => {
      let ownBoxes: any[] = [];
      let enemyBoxes: any[] = [];
      let isVictoryOrDefeat: boolean = false;

      gameBoxes.forEach((gameBox: any) => {
        if (gameBox.campChoice === playerSide) ownBoxes.push(gameBox.box);
        else if (gameBox.campChoice === enemySide) enemyBoxes.push(gameBox.box);
      });

      if (ownBoxes.length >= 3) {
        victoryCases.forEach((item) => {
          const containsAll = item.every((element) => {
            return ownBoxes.includes(element);
          });
          if (containsAll) {
            setIsVictory(true);
            isVictoryOrDefeat = true;
          }
        });
      }
      if (enemyBoxes.length >= 3) {
        victoryCases.forEach((item) => {
          const containsAll = item.every((element) => {
            return enemyBoxes.includes(element);
          });
          if (containsAll) {
            setIsDefeat(true);
            isVictoryOrDefeat = true;
          }
        });
      }
      return isVictoryOrDefeat;
    },
    [playerSide, enemySide]
  );

  const playTurn = useCallback(
    (opponentType: string, box: any = null, side: any = null) => {
      //Make a copy of allBoxes

      let newAllBoxes = allBoxes.map((a) => {
        return { ...a };
      });

      const newAvailableBoxes = availableBoxes.map((a) => {
        return { ...a };
      });

      //console.log("availableBoxes", availableBoxes);
      //if(availableBoxes.length)

      //Define turn (if player first or if ia first)

      ticTacToeHelper(playerBoxes, iaBoxes, availableBoxes);

      const randomBox =
        availableBoxes[Math.floor(Math.random() * availableBoxes.length)];

      let findEmptyBox = newAllBoxes.find((a) => {
        if (opponentType === "player") {
          setPlayerBoxes([...playerBoxes, box]);
          return a.box === box;
        } else {
          setIaBoxes([...iaBoxes, randomBox.box]);
          return a.box === randomBox.box;
        }
      });

      if (!findEmptyBox.campChoice.length) {
        findEmptyBox.campChoice = opponentType === "player" ? side : enemySide;

        const indexOfObject = availableBoxes.findIndex((object) => {
          if (opponentType === "player") {
            return object.box === box;
          } else {
            return object.box === randomBox.box;
          }
        });
        newAvailableBoxes.splice(indexOfObject, 1);
        /*         console.log(newAvailableBoxes);
        console.log([newAvailableBoxes]);
        console.log([...newAvailableBoxes]);
        console.log(availableBoxes); */
        setAvailableBoxes([...newAvailableBoxes]);
      }

      const isGameDone: boolean = checkVictory(newAllBoxes);
      if (!isGameDone) {
        setAllBoxes(newAllBoxes);

        if (opponentType === "ia") {
          setIsPlayerTurn(true);
          setIsIaTurn(false);
        } else {
          setIsPlayerTurn(false);
          setIsIaTurn(true);
        }
      }
    },
    [allBoxes, availableBoxes, checkVictory, enemySide, iaBoxes, playerBoxes]
  );

  useEffect(() => {
    if (isIaTurn && isGameStarted) {
      playTurn("ia");
    }
  }, [playTurn, isIaTurn, isGameStarted]);

  const changeBox = useCallback(
    (box: any, side: any) => {
      if (isPlayerTurn && isGameStarted) {
        playTurn("player", box, side);
      }
    },
    [playTurn, isPlayerTurn, isGameStarted]
  );

  return {
    chooseCross,
    chooseRound,
    playerSide,
    isVictory,
    isDefeat,
    startGame,
    allBoxes,
    isGameStarted,
    changeBox,
    firstPlayer,
    rollSide,
    resetAll,
  };
}
