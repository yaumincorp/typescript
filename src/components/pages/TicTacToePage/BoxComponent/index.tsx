type Box = {
  campChoice: string;
  box: string;
};

type BoxComponentProps = {
  isGameStarted: boolean;
  changeBox: (box: any, playerSide: any) => void;
  boxItem: Box;
  playerSide: "cross" | "round" | null;
};

const BoxComponent = ({
  isGameStarted,
  changeBox,
  boxItem,
  playerSide,
}: BoxComponentProps) => {
  const campChoice = boxItem.campChoice === "cross" ? "X" : "O";
  return (
    <div
      className={`border-b-4 border-r-4 border-indigo-600 h-24 w-24 flex items-center justify-center${
        !isGameStarted ? "disable" : ""
      }`}
      onClick={() => {
        changeBox(boxItem.box, playerSide);
      }}
    >
      <div className="text-5xl">
        {boxItem.campChoice.length ? campChoice : ""}
      </div>
    </div>
  );
};

export default BoxComponent;
