import HangmanPage from "..";
import {
  fireEvent,
  getByText,
  render,
  renderHook,
  screen,
} from "@testing-library/react";
import { useUser } from "../../../../hooks/query/user/useUser";
import { useWord } from "../../../../hooks/query/useWord";
import { QueryClient, QueryClientProvider } from "react-query";

// Solves TypeScript Errors
const mockedUseUser = useUser as jest.Mock<any>;
const mockedUseWord = useWord as jest.Mock<any>;
const mockedNavigator = jest.fn();

const object = jest.requireActual("react-router-dom");

jest.mock("react-router-dom", () => ({
  ...object,
  useNavigate: () => ({
    navigate: mockedNavigator,
  }),
  useParams: () => ({
    difficulty: "easy",
    userId: "6298b889914b055ff84d72a9",
  }),
  useRouteMatch: () => ({ url: "/hangman/6298b889914b055ff84d72a9/easy" }),
}));
// Mock the module
jest.mock("../../../../hooks/query/user/useUser");
jest.mock("../../../../hooks/query/useWord");

const localStorageMock = (() => {
  let store: any = {};

  return {
    getItem(key: any) {
      return store[key] || null;
    },
    setItem(key: any, value: any) {
      store[key] = value.toString();
    },
    removeItem(key: any) {
      delete store[key];
    },
    clear() {
      store = {};
    },
  };
})();

Object.defineProperty(window, "sessionStorage", {
  value: localStorageMock,
});

describe("Hangman page render test", () => {
  beforeEach(() => {
    window.sessionStorage.clear();
    jest.restoreAllMocks();
  });

  beforeEach(() => {
    mockedUseUser.mockImplementation(() => ({ isLoading: true, data: [] }));
    mockedUseWord.mockImplementation(() => ({ isLoading: true }));
  });
  afterEach(() => {
    jest.clearAllMocks();
  });

  /*const getItemSpy = jest.spyOn(window.sessionStorage, "getItem");
    window.sessionStorage.setItem(
      "token ",
      JSON.stringify({
        token: "a6a3ccd8-cbcd-4ac7-9c92-d2a63b17541a",
        userId: "6298b889914b055ff84d72a9",
      })
    );

    console.log("GET ITEM SPY", getItemSpy); */
  /* 
    const wrapper = ({ children }: any) => (
      <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
    );

    const { result, waitFor } = renderHook(() => useUser("1"), { wrapper });

    await waitFor(() => result.current.isSuccess);

    expect(result.current.data).toEqual("Hello"); */
  it("Render without error", async () => {
    const queryClient = new QueryClient();

    render(
      <QueryClientProvider client={queryClient}>
        <HangmanPage routeName="Hangman" />
      </QueryClientProvider>
    );

    // Fetches a default product when `productId` isn't specified (id="1")
    expect(useUser).toHaveBeenCalledWith(undefined);
    expect(useUser).toHaveBeenCalledTimes(2);

    //expect(useWord).toHaveBeenCalledWith("ARBORICOLE");
  });

  it.skip("Render without error, start game and do nothing", () => {
    render(<HangmanPage routeName="Hangman" />);

    fireEvent.click(screen.getByTestId("button-game-start"));
  });

  it.skip("Render without error, start game and set easy mode", () => {
    render(<HangmanPage routeName="Hangman" />);

    fireEvent.click(screen.getByTestId("button-set-easy"));

    fireEvent.click(screen.getByTestId("button-game-start"));
  });

  it.skip("Render without error, start game and set medium mode", () => {
    render(<HangmanPage routeName="Hangman" />);

    fireEvent.click(screen.getByTestId("button-set-medium"));

    fireEvent.click(screen.getByTestId("button-game-start"));
  });

  it.skip("Render without error, start game and set hard mode", () => {
    render(<HangmanPage routeName="Hangman" />);

    fireEvent.click(screen.getByTestId("button-set-hard"));

    fireEvent.click(screen.getByTestId("button-game-start"));
  });

  it.skip("Render without error, start game and set easy mode and word found", () => {
    render(<HangmanPage routeName="Hangman" />);

    fireEvent.click(screen.getByTestId("button-set-easy"));

    fireEvent.click(screen.getByTestId("button-game-start"));

    const a = fireEvent.click(
      screen.getAllByTestId("button-letter-component")[0]
    );
    const b = fireEvent.click(
      screen.getAllByTestId("button-letter-component")[1]
    );
    const c = fireEvent.click(
      screen.getAllByTestId("button-letter-component")[2]
    );
    const d = fireEvent.click(
      screen.getAllByTestId("button-letter-component")[3]
    );
  });
});
