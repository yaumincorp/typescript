import { useCallback, useEffect, useMemo, useState } from "react";
import { useNavigate } from "react-router-dom";
import { createScore, updateScore } from "../../../api/hangman";
import { useUser } from "../../../hooks/query/user/useUser";
import { useWord } from "../../../hooks/query/useWord";

/* eslint import/no-anonymous-default-export: [2, {"allowAnonymousFunction": true}] */
export default function (tokenUserId: string, userId: string) {
  const { data: userIdData, isError } = useUser(tokenUserId);

  //LOAD DATA WORD

  const { data: randomList, isLoading, error } = useWord();

  let navigate = useNavigate();

  //useStates

  const [randomWord, setRandomWord] = useState<string>("");

  const [nbWord, setNbWord] = useState<number>(0);
  const [nbLifes, setNbLifes] = useState<number>();
  const [alreadyTypedLetter, setAlreadyTypedLetter] = useState<Array<string>>(
    []
  );
  const [suiteWord, setSuiteWord] = useState<Array<string>>([]);
  const [isGameStarted, setIsGameStarted] = useState<boolean>(false);

  /* Difficulties */
  const [difficultyMode, setDifficultyMode] = useState<
    "easy" | "medium" | "hard" | null
  >(null);

  const [currentDifficultyMode, setCurrentDifficultyMode] = useState<
    "easy" | "medium" | "hard" | null
  >(null);

  //Clear function

  //useMemos

  const generateAlphabet = useMemo(() => {
    let arrayAlphabet = [];

    for (let i = "a".charCodeAt(0); i <= "z".charCodeAt(0); i++) {
      arrayAlphabet.push(String.fromCharCode(i).toUpperCase());
    }

    return arrayAlphabet;
  }, []);

  const isGameOver = useMemo<boolean>(() => {
    return nbLifes === 0;
  }, [nbLifes]);

  const isSuccess = useMemo(() => {
    console.log("COUCOU", randomWord);
    const wordToFound = Array.from(randomWord);
    return (
      wordToFound.length &&
      wordToFound
        .filter((letter, index) => {
          if (difficultyMode === "easy") return index !== 0 && letter;
          else return letter;
        })
        .every((letter) => alreadyTypedLetter.includes(letter))
    );
  }, [randomWord, alreadyTypedLetter, difficultyMode]);

  //useCallbacks

  const startNewGame = useCallback(() => {
    /*
      Si je lance une partie, je génère un mot
      Si je continue ma difficulté, je garde mon mot
      Si je change ma difficulté et que je lance une partie, je génère un mot
    */

    setAlreadyTypedLetter([]);
    setNbWord(1);

    switch (difficultyMode) {
      case "easy":
        setNbLifes(7);
        break;
      case "medium":
        setNbLifes(5);
        break;
      case "hard":
        setNbLifes(3);
        break;
      default:
        break;
    }

    //Generate word
    const randomFilterArray = randomList?.filter((item) => {
      console.log(item);
      return !suiteWord.includes(item);
    });

    setRandomWord(
      randomFilterArray[Math.floor(Math.random() * randomFilterArray.length)]
    );

    setIsGameStarted(true);
    setCurrentDifficultyMode(difficultyMode);
    navigate(`/hangman/${tokenUserId}/${difficultyMode}`);
  }, [
    tokenUserId,
    randomList,
    difficultyMode,
    setNbLifes,
    suiteWord,
    setIsGameStarted,
    setCurrentDifficultyMode,
    navigate,
  ]);

  const nextWord = useCallback(() => {
    setAlreadyTypedLetter([]);
    setNbWord((prevState) => (isGameOver ? 1 : prevState + 1));

    switch (difficultyMode) {
      case "easy":
        setNbLifes(7);
        break;
      case "medium":
        setNbLifes(5);
        break;
      case "hard":
        setNbLifes(3);
        break;
      default:
        break;
    }

    //Generate word
    const randomFilterArray = randomList.filter((item) => {
      return !suiteWord.includes(item);
    });

    setRandomWord(
      randomFilterArray[Math.floor(Math.random() * randomFilterArray.length)]
    );
  }, [
    randomList,
    setNbWord,
    setRandomWord,
    suiteWord,
    isGameOver,
    difficultyMode,
  ]);

  //useEffects

  useEffect(() => {
    setSuiteWord((prevState) => {
      if (randomWord) {
        return [...prevState, randomWord];
      }
      return [];
    });
  }, [randomWord, setSuiteWord]);

  useEffect(() => {
    if (isGameOver) {
      if (
        difficultyMode === userIdData.difficulty &&
        nbWord > userIdData.score
      ) {
        updateScore(userIdData.scoreId, difficultyMode, nbWord - 1);
      } else if (difficultyMode !== userIdData.difficulty && nbWord > 1) {
        createScore(tokenUserId, difficultyMode, nbWord - 1);
      }
    }
  }, [nbWord, isGameOver, difficultyMode, tokenUserId, userIdData]);

  return {
    randomWord,
    generateAlphabet,
    nbWord,
    nbLifes,
    setNbLifes,
    alreadyTypedLetter,
    setAlreadyTypedLetter,
    isGameStarted,
    setIsGameStarted,
    difficultyMode,
    setDifficultyMode,
    isSuccess,
    isGameOver,
    currentDifficultyMode,
    setCurrentDifficultyMode,
    startNewGame,
    nextWord,
  };
}
