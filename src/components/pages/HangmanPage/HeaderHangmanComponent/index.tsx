type HeaderHangmanProps = {
  nbWord: number;
  nbLetters: number;
  nbLifes: number;
};

const HeaderHangmanComponent = ({
  nbWord,
  nbLetters,
  nbLifes,
}: HeaderHangmanProps) => {
  return (
    <div className="flex flex-row justify-between my-4">
      <div className="flex">{nbWord}er mot</div>
      <h2>Il y a {nbLetters} lettres dans ce mot</h2>
      <div className="flex flex-row">
        <p>Lifes: </p>
        {Array.from(Array(nbLifes)).map((_, lifeIndex) => (
          <div key={lifeIndex} className="life mx-1" />
        ))}
      </div>
    </div>
  );
};

export default HeaderHangmanComponent;
