import classNames from "classnames";
import { useMemo } from "react";

type LetterProps = {
  letter: string;
  wordToFound: string;
  alreadyTypedLetter: Array<string>;
  difficultyMode: string;
} & React.DetailedHTMLProps<
  React.ButtonHTMLAttributes<HTMLButtonElement>,
  HTMLButtonElement
>;

const LetterComponent = ({
  letter,
  wordToFound,
  alreadyTypedLetter,
  difficultyMode,
  ...props
}: LetterProps) => {
  console.log("WORD TO FOUND", wordToFound);

  /*@TODO: -
  règles mode facile
  si première lettre affiché et cette lettre pas utilisé ailleurs, si click, affichage en rouge et perte de points
  si première lettre affiché et cette lettre utilisé ailleurs, si click, affichage en vert
  */

  const displayColorLetter = useMemo(() => {
    if (
      alreadyTypedLetter.includes(letter) &&
      difficultyMode === "easy" &&
      wordToFound.includes(letter, 1)
    ) {
      return "bg-green-600";
    } else if (
      alreadyTypedLetter.includes(letter) &&
      difficultyMode !== "easy" &&
      wordToFound.includes(letter)
    ) {
      return "bg-slate-500";
    } else if (
      alreadyTypedLetter.includes(letter) &&
      !wordToFound.includes(letter)
    ) {
      return "bg-red-600";
    } else {
      return "bg-slate-500";
    }
  }, [alreadyTypedLetter, wordToFound, letter, difficultyMode]);

  return (
    <button
      type="button"
      data-testid="button-letter-component"
      className={classNames(
        "p-2 rounded-lg shadow-lg text-white",
        displayColorLetter
      )}
      {...props}
    >
      {letter}
    </button>
  );
};

export default LetterComponent;
