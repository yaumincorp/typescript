import LetterComponent from "..";
import { render } from "@testing-library/react";

describe("LetterComponent page render test", () => {
  it("Render without error, easy mode", () => {
    render(
      <LetterComponent
        letter="A"
        wordToFound="ARBORICOLE"
        alreadyTypedLetter={["A", "B", "C", "E", "I", "L", "O", "R"]}
        difficultyMode="easy"
      />
    );
  });

  it("Render without error, medium mode", () => {
    render(
      <LetterComponent
        letter="A"
        wordToFound="ARBORICOLE"
        alreadyTypedLetter={["A", "B", "C", "E", "I", "L", "O", "R"]}
        difficultyMode="medium"
      />
    );
  });
});
