import { useWord } from "../../../hooks/query/useWord";
import { AppProps } from "../../../interfaces";
import HeaderHangmanComponent from "./HeaderHangmanComponent";
import LetterComponent from "./LetterComponent";
import MenuHangmanComponent from "./MenuHangmanComponent";
import WordComponent from "./WordComponent";
import useHook from "./hook";
import { useParams } from "react-router-dom";
import { useState } from "react";

const HangmanPage = ({ routeName, tokenUserId }: AppProps) => {
  const { difficulty, userId: urlUserId } = useParams();

  const {
    randomWord,
    generateAlphabet,
    nbWord,
    nbLifes,
    setNbLifes,
    alreadyTypedLetter,
    setAlreadyTypedLetter,
    setIsGameStarted,
    isGameStarted,
    difficultyMode,
    setDifficultyMode,
    isSuccess,
    isGameOver,
    currentDifficultyMode,
    setCurrentDifficultyMode,
    startNewGame,
    nextWord,
  } = useHook(tokenUserId, urlUserId);

  return (
    <div className="px-4 py-4">
      <h1 className="text-3xl font-bold underline my-2">{routeName}</h1>

      {!urlUserId ? (
        <MenuHangmanComponent
          difficultyMode={difficultyMode}
          setDifficultyMode={setDifficultyMode}
          setIsGameStarted={setIsGameStarted}
          isGameStarted={isGameStarted}
          userId={tokenUserId}
          currentDifficultyMode={currentDifficultyMode}
          startNewGame={startNewGame}
        />
      ) : (
        <div className="px-2 py-2">
          <HeaderHangmanComponent
            nbWord={nbWord}
            nbLetters={randomWord.length}
            nbLifes={nbLifes}
          />
          <WordComponent
            difficultyMode={difficultyMode}
            wordToFound={Array.from(randomWord)}
            alreadyTypedLetter={alreadyTypedLetter}
          />

          <div>
            {isSuccess && tokenUserId && (
              <div className="flex flex-col items-center">
                <div className="w-40 my-2 p-2 rounded-lg shadow-md bg-green-300">
                  SUCCESS
                </div>

                <button
                  className="button"
                  onClick={() => {
                    nextWord();
                  }}
                >
                  Mot suivant
                </button>
              </div>
            )}

            {isGameOver && (
              <>
                <div className="p-2 rounded-lg shadow-lg bg-red-700">
                  GAME OVER
                </div>
                <button className="button" onClick={() => startNewGame()}>
                  Réessayer
                </button>
              </>
            )}

            {!isSuccess && !isGameOver && (
              <div className="grid grid-cols-10 gap-4 hover:gap-4 my-2">
                {generateAlphabet.map((letter: string, index: number) => (
                  <LetterComponent
                    //change the key
                    key={index}
                    letter={letter}
                    wordToFound={randomWord}
                    alreadyTypedLetter={alreadyTypedLetter}
                    onClick={() => {
                      setAlreadyTypedLetter((prevState) => {
                        return [...prevState, letter];
                      });
                      setNbLifes((prevState) => {
                        if (!randomWord.includes(letter)) return prevState - 1;
                        return prevState;
                      });
                    }}
                    difficultyMode={difficultyMode}
                  />
                ))}
              </div>
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default HangmanPage;
