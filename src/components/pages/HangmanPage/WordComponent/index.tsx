import { useEffect } from "react";

type WordProps = {
  wordToFound: Array<string>;
  difficultyMode: string;
  alreadyTypedLetter: Array<string>;
};

const WordComponent = ({
  wordToFound,
  difficultyMode,
  alreadyTypedLetter,
}: WordProps) => {
  return (
    <div className="flex flex-row justify-center my-4">
      {difficultyMode === "easy" && (
        <p className="px-2 border border-indigo-500 text-lg font-bold">
          {wordToFound[0]}
        </p>
      )}
      {/* @TODO: - Utiliser une key avec l'index et autre chose */}
      {wordToFound.map((letter, index) => {
        if (difficultyMode === "easy") {
          return (
            index !== 0 && (
              <p className="px-2 border border-indigo-500">
                {alreadyTypedLetter.includes(letter) ? letter : "_"}
              </p>
            )
          );
        } else {
          return (
            <p className="px-2 border border-indigo-500">
              {alreadyTypedLetter.includes(letter) ? letter : "_"}
            </p>
          );
        }
      })}
    </div>
  );
};
export default WordComponent;
