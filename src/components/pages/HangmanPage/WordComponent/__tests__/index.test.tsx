import WordComponent from "..";
import { render } from "@testing-library/react";

describe("Word component render test", () => {
  it("Render without error, easy mode and alreadyTypedLetter empty", () => {
    render(
      <WordComponent
        wordToFound={["M", "O", "T"]}
        difficultyMode="easy"
        alreadyTypedLetter={[]}
      />
    );
  });

  it("Render without error, medium mode and alreadyTypedLetter empty", () => {
    render(
      <WordComponent
        wordToFound={["M", "O", "T"]}
        difficultyMode="medium"
        alreadyTypedLetter={[]}
      />
    );
  });

  it("Render without error, easy mode", () => {
    render(
      <WordComponent
        wordToFound={["M", "O", "T"]}
        difficultyMode="easy"
        alreadyTypedLetter={["O", "M", "T"]}
      />
    );
  });

  it("Render without error, medium mode", () => {
    render(
      <WordComponent
        wordToFound={["M", "O", "T"]}
        difficultyMode="medium"
        alreadyTypedLetter={["O", "M", "T"]}
      />
    );
  });

  it("Render without error, hard mode", () => {
    render(
      <WordComponent
        wordToFound={["M", "O", "T"]}
        difficultyMode="hard"
        alreadyTypedLetter={["O", "M", "T"]}
      />
    );
  });
});
