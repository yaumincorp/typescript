import { Link, useNavigate } from "react-router-dom";
import useModal from "../../../shared/Modal/hook";
import Modal from "../../../shared/Modal/modal";

type difficulty = "easy" | "medium" | "hard";

type MenuHangmanProps = {
  difficultyMode: difficulty;
  setDifficultyMode: (difficultyMode: difficulty) => void;
  setIsGameStarted: (isGameStarted: boolean) => void;
  isGameStarted: boolean;
  userId: string;
  currentDifficultyMode: difficulty;
  startNewGame: () => void;
};

const MenuHangmanComponent = ({
  difficultyMode,
  setDifficultyMode,
  setIsGameStarted,
  isGameStarted,
  userId,
  currentDifficultyMode,
  startNewGame,
}: MenuHangmanProps) => {
  let navigate = useNavigate();

  const { isShowing, toggle } = useModal();

  const continueGame = () => {
    setIsGameStarted(true);
    navigate(`/hangman/${userId}/${difficultyMode}`);
  };

  return (
    <div className="px-4 py-4">
      <div className="px-6 py-6">
        <button
          data-testid="button-set-easy"
          data-tooltip-target="tooltip-easy"
          className={`mx-2  ${
            difficultyMode === "easy" && "border-2 border-indigo-600"
          } text-white bg-green-500 hover:bg-green-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800`}
          onClick={() => {
            setDifficultyMode("easy");
          }}
        >
          EASY
        </button>
        <div
          id="tooltip-easy"
          role="tooltip"
          className="inline-block absolute invisible z-10 py-2 px-3 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip dark:bg-gray-700"
        >
          Mode facile : 7 vies et les lettres qui ne matchent pas sont affichées
          en rouge
          <div className="tooltip-arrow" data-popper-arrow></div>
        </div>
        <button
          data-testid="button-set-medium"
          data-tooltip-target="tooltip-medium"
          className={`mx-2 ${
            difficultyMode === "medium" && "border-2 border-indigo-600"
          } text-white bg-slate-500 hover:bg-slate-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800`}
          onClick={() => {
            setDifficultyMode("medium");
          }}
        >
          MEDIUM
        </button>
        <div
          id="tooltip-medium"
          role="tooltip"
          className="inline-block absolute invisible z-10 py-2 px-3 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip dark:bg-gray-700"
        >
          Mode moyen : 5 vies mais pas de première lettre
          <div className="tooltip-arrow" data-popper-arrow></div>
        </div>

        <button
          data-testid="button-set-hard"
          data-tooltip-target="tooltip-hard"
          className={`mx-2 ${
            difficultyMode === "hard" && "border-2 border-indigo-600"
          } text-white bg-red-700 hover:bg-red-900 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800`}
          onClick={() => {
            setDifficultyMode("hard");
          }}
        >
          HARD
        </button>

        <div
          id="tooltip-hard"
          role="tooltip"
          className="inline-block absolute invisible z-10 py-2 px-3 text-sm font-medium text-white bg-gray-900 rounded-lg shadow-sm opacity-0 transition-opacity duration-300 tooltip dark:bg-gray-700"
        >
          Mode difficle : 3 vies mais pas de première lettre
          <div className="tooltip-arrow" data-popper-arrow></div>
        </div>
      </div>
      <div>
        <button
          data-testid="button-game-start"
          className="button"
          onClick={() => {
            if (!isGameStarted) startNewGame();
            if (isGameStarted && difficultyMode === currentDifficultyMode)
              continueGame();
            if (isGameStarted && difficultyMode !== currentDifficultyMode)
              toggle();
          }}
        >
          {/* 
          
            Si la partie n'a pas commencé, j'affiche start
            Si la partie a commencé et que le mode de difficulté sélectionné
            est différent du mode de difficulté de la partie en cours, j'affiche start
            Si la partie est commencé, et que le mode de difficulté sélectionné
            est le même, alors j'affiche continue 
          */}

          {!isGameStarted ||
          (isGameStarted && difficultyMode !== currentDifficultyMode)
            ? "START"
            : "CONTINUE"}
        </button>
      </div>
      <Modal
        isShowing={isShowing}
        hide={toggle}
        titleHeader="Commencer une nouvelle partie ?"
        bodyText="Vous avez déjà une partie en cours. Vous allez commencer une nouvelle partie. Êtes-vous sûr ?"
        isFooterButtons
        firstFunction={startNewGame}
      />
    </div>
  );
};

export default MenuHangmanComponent;
