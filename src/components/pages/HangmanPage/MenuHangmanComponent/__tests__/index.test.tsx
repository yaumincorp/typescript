import MenuHangmanComponent from "..";
import { fireEvent, getByText, render, screen } from "@testing-library/react";

const mockedNavigator = jest.fn();

const object = jest.requireActual("react-router-dom");

jest.mock("react-router-dom", () => ({
  ...object,
  useNavigate: () => ({
    navigate: mockedNavigator,
  }),
}));

describe("MenuHangmanComponent page render test", () => {
  it("Render without error, set easy mode", () => {
    render(
      <MenuHangmanComponent
        difficultyMode="easy"
        setDifficultyMode={jest.fn()}
        setIsGameStarted={jest.fn()}
        isGameStarted={true}
        userId="0ZKE3E3KV"
        currentDifficultyMode="easy"
        startNewGame={jest.fn()}
      />
    );

    fireEvent.click(screen.getByTestId("button-set-easy"));
  });

  it("Render without error, set medium mode", () => {
    render(
      <MenuHangmanComponent
        difficultyMode="medium"
        setDifficultyMode={jest.fn()}
        setIsGameStarted={jest.fn()}
        isGameStarted={true}
        userId="0ZKE3E3KV"
        currentDifficultyMode="easy"
        startNewGame={jest.fn()}
      />
    );

    fireEvent.click(screen.getByTestId("button-set-medium"));
  });

  it("Render without error, set hard mode", () => {
    render(
      <MenuHangmanComponent
        difficultyMode="hard"
        setDifficultyMode={jest.fn()}
        setIsGameStarted={jest.fn()}
        isGameStarted={true}
        userId="0ZKE3E3KV"
        currentDifficultyMode="easy"
        startNewGame={jest.fn()}
      />
    );

    fireEvent.click(screen.getByTestId("button-set-hard"));
  });
});
