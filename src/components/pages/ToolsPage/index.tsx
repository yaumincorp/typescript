import { createRef, useEffect, useRef, useState } from "react";
import socketIOClient from "socket.io-client";
import io from "socket.io-client";

type ToolsPageProps = {
  routeName: string;
};

const ToolsPage = ({ routeName }: ToolsPageProps) => {
  let localVideoRef = useRef<HTMLVideoElement>(null);
  let partnerVideoRef = useRef<HTMLVideoElement>(null);
  const [stream, setStream] = useState<MediaStream>();

  const [startVideo, setStartVideo] = useState(false);
  const [startTime, setStartTime] = useState(0);

  const callRemoteVideo = () => {
    console.log("starting call");

    setStartTime(window.performance.now);

    const videoTracks = stream.getVideoTracks();
    const audioTracks = stream.getAudioTracks();

    if (videoTracks.length > 0) {
      console.log(`Using video device: ${videoTracks[0].label}`);
    }
    if (audioTracks.length > 0) {
      console.log(`Using audio device: ${audioTracks[0].label}`);
    }

    const configuration = {};
    console.log("RTCPeerConnection configuration:", configuration);
  };

  useEffect(() => {
    navigator.mediaDevices
      .getUserMedia({ video: true, audio: false })
      .then((streamUser) => {
        if (localVideoRef.current) {
          localVideoRef.current.srcObject = streamUser;
          setStream(streamUser);
          localVideoRef.current.onloadedmetadata = () => {
            localVideoRef.current.play();
          };
        }
        const track = streamUser.getTracks()[0];

        if (!startVideo) track.stop();
      });
  }, [startVideo]);

  return (
    <div className="md:container md:mx-auto ">
      <h1 className="text-3xl font-bold underline">{routeName}</h1>
      <div className="flex">
        <div>
          <video
            playsInline
            style={{
              width: 480,
              height: 480,
              margin: 5,
              backgroundColor: "black",
            }}
            muted
            ref={localVideoRef}
            autoPlay
          ></video>
          <button
            className="button"
            onClick={() => {
              setStartVideo(!startVideo);
            }}
          >
            {!startVideo ? "Start" : "Stop"}
          </button>
        </div>
        <div>
          <video
            playsInline
            style={{
              width: 480,
              height: 480,
              margin: 5,
              backgroundColor: "black",
            }}
            muted
            ref={partnerVideoRef}
            autoPlay
          ></video>
        </div>
      </div>
    </div>
  );
};

export default ToolsPage;
