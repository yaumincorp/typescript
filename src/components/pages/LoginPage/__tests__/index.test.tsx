import LoginPage from "..";
import {
  cleanup,
  fireEvent,
  getByText,
  render,
  screen,
} from "@testing-library/react";

import { QueryClient, QueryClientProvider } from "react-query";

describe("Login page render test", () => {
  it.skip("Render without error, display form", () => {
    const queryClient = new QueryClient();
    const setToken = jest.fn();
    render(
      <QueryClientProvider client={queryClient}>
        <LoginPage routeName="Home" setToken={setToken} />
      </QueryClientProvider>
    );

    fireEvent.click(screen.getByText("Next"));

    fireEvent.click(screen.getByText("Next"));

    const element = screen.getByTestId("form-title");

    expect(element).toBeInTheDocument();

    const inputTitle = screen.getByTestId("input-title");

    fireEvent.change(inputTitle, { target: { value: "Test" } });

    expect(inputTitle.textContent).toBe("");

    fireEvent.click(screen.getByText(/Upload post/i));
  });
});
