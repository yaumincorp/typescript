import { SetStateAction, useState } from "react";
import { useNavigate } from "react-router-dom";
import { checkUser, loginUser } from "../../../api/user";
import HttpsIcon from "@mui/icons-material/Https";
import PersonIcon from "@mui/icons-material/Person";
import Input from "../../shared/Input/input";

type LoginProps = {
  routeName: string;
  setToken: (token: any) => void;
};

const LoginPage = ({ routeName, setToken }: LoginProps) => {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  let navigate = useNavigate();

  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    let token = {};

    const { result, user } = await checkUser({
      userName,
      password,
    });

    if (result) {
      token = await loginUser({
        userName,
        password,
      });
    }

    const userId = user._id;

    setToken({ ...token, userId });

    setUserName("");
    setPassword("");

    navigate("/");
  };

  const handleChangeUserName = (e: {
    target: { value: SetStateAction<string> };
  }) => {
    setUserName(e.target.value);
  };

  const handleChangePassword = (e: {
    target: { value: SetStateAction<string> };
  }) => {
    setPassword(e.target.value);
  };

  return (
    <div className="md:container md:mx-auto">
      <h1 className="text-3xl font-bold underline">{routeName}</h1>
      <div className="my-4">
        <form className="flex flex-col items-center" onSubmit={onSubmit}>
          <Input
            id="username"
            type="text"
            name="username"
            placeholder="Username"
            dataTestid="input-username"
            icon={<PersonIcon className="text-xl text-violet-400" />}
            onChange={handleChangeUserName}
            onBlur={() => {}}
            value={userName}
          />
          <Input
            id="password"
            type="password"
            name="password"
            placeholder="Password"
            dataTestid="input-password"
            icon={<HttpsIcon className="text-xl text-violet-400" />}
            onChange={handleChangePassword}
            onBlur={() => {}}
          />
          <button className="button" type="submit">
            Login
          </button>
        </form>
      </div>
    </div>
  );
};

export default LoginPage;
