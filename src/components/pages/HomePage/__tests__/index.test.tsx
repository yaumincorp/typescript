import HomePage from "..";
import {
  cleanup,
  fireEvent,
  getByText,
  render,
  screen,
} from "@testing-library/react";

import { QueryClient, QueryClientProvider } from "react-query";

afterEach(cleanup);

describe("Home page render test", () => {
  it("Render without error", () => {
    const queryClient = new QueryClient();

    render(
      <QueryClientProvider client={queryClient}>
        <HomePage routeName="Home" />
      </QueryClientProvider>
    );
  });

  it("Render without error, click on next", () => {
    const queryClient = new QueryClient();
    render(
      <QueryClientProvider client={queryClient}>
        <HomePage routeName="Home" />
      </QueryClientProvider>
    );

    fireEvent.click(screen.getByText("Next"));

    const element = screen.getByTestId("home-page-user");

    expect(element).toBeInTheDocument();
  });

  it("Render without error, click on fetchUser", () => {
    const queryClient = new QueryClient();
    render(
      <QueryClientProvider client={queryClient}>
        <HomePage routeName="Home" />
      </QueryClientProvider>
    );

    fireEvent.click(screen.getByText("Next"));

    const element = screen.getByTestId("home-page-user");

    expect(element).toBeInTheDocument();
  });
});
