import { useEffect, useState } from "react";
import { useUsers } from "../../../hooks/query/user/useUser";
import { AppProps, User } from "../../../interfaces";
import { DataGrid } from "./DataGrid";

const HomePage = ({ routeName }: AppProps) => {
  const [user, setUser] = useState<User | null>(null);

  const fetchUser = () => {
    /*     setUser({
      firstName: "Bastien",
      lastName: "Ruchon",
      pseudo: "bruchon",
      adress: {
        street: "Rue Claude Cornac",
        number: 3,
        zip: "31140",
      },
      age: 30,
      admin: false,
    }); */
  };

  //enums
  enum HomePageStep {
    Routes = "Routes",
    User = "User",
    Post = "Post",
  }

  const [homePageStep, setHomePageStep] = useState<HomePageStep>(
    HomePageStep.Routes
  );

  const [limit, setLimit] = useState<number>(2);
  const [skip, setSkip] = useState<number>(0);

  const nextPage = () => {
    setSkip(skip + limit);
  };

  const previousPage = () => {
    setSkip(skip - limit);
  };

  const { isLoading, isError, error, data, isFetching, isPreviousData } =
    useUsers(limit, skip);

  console.log("DATA USERS", data);

  return (
    <div className="md:container md:mx-auto text-xs sm:text-base">
      <h1 className="text-3xl font-bold underline" data-testid="home-page-name">
        {routeName}
      </h1>
      {homePageStep === HomePageStep.Routes && (
        <div>
          <DataGrid items={data?.users} />

          <div className="flex flex-col items-center">
            <div className="flex flex-row items-center py-4">
              <button
                className={`button ${skip <= 0 ? "disable" : ""} mx-2`}
                onClick={previousPage}
              >
                Previous Page
              </button>

              <button
                className={`button ${
                  isPreviousData || !data?.isMore ? "disable" : ""
                } mx-2`}
                onClick={nextPage}
              >
                Next Page
              </button>
            </div>
            <button
              type="button"
              className="button"
              onClick={() => setHomePageStep(HomePageStep.User)}
            >
              Next
            </button>
          </div>
        </div>
      )}

      {homePageStep === HomePageStep.User && (
        <div data-testid="home-page-user">
          <button type="button" onClick={fetchUser}>
            Get user
          </button>
          {/* <div>
            {user && (
              <p data-testid="user-first-name">Bienvenue {user.firstName}</p>
            )}
          </div> */}
          <button
            type="button"
            className="button"
            onClick={() => setHomePageStep(HomePageStep.Post)}
          >
            Next
          </button>
        </div>
      )}

      {homePageStep === HomePageStep.Post && (
        <>
          <h1>Form</h1>
          <p>Create a post</p>

          <button
            type="button"
            className="button"
            onClick={() => setHomePageStep(HomePageStep.Routes)}
          >
            Next
          </button>
        </>
      )}
    </div>
  );
};

export default HomePage;
