import { User } from "../../../../interfaces";

interface DataGridProps<T> {
  items: User[];
}

export function DataGrid<T extends User>({ items }: DataGridProps<T>) {
  return (
    <>
      <div className="grid grid-cols-5 gap-4 hover:gap-6 my-4">
        <div className="p-2 rounded-lg shadow-lg bg-rose-400">{"Name"}</div>
        <div className="p-2 rounded-lg shadow-lg bg-slate-500">
          {"Lastname"}
        </div>
        <div className="p-2 rounded-lg shadow-lg bg-sky-700">{"Username"}</div>
        <div className="p-2 rounded-lg shadow-lg bg-fuchsia-500">{"Age"}</div>
        <div className="p-2 rounded-lg shadow-lg bg-green-300">{"Record"}</div>
      </div>
      <div className="">
        {/* className="overflow-y-scroll" */}
        {items &&
          items.map((item, index) => {
            const records = item?.score;

            let newScore: any[] = records.map(
              ({ _id, userId, ...keepAttrs }) => keepAttrs
            );

            let emptyScores = [
              { title: "Easy", type: "easy", score: 0 },
              { title: "Medium", type: "medium", score: 0 },
              { title: "Hard", type: "hard", score: 0 },
            ];

            const allRecords = emptyScores.reduce((acc, curr) => {
              const stored = newScore.find(
                ({ difficulty }) => difficulty === curr.type
              );
              if (stored) {
                acc.push({
                  title: stored.difficulty,
                  type: stored.difficulty,
                  score: stored.score,
                });
              } else {
                acc.push(curr);
              }
              return acc;
            }, []);

            return (
              <div
                className="grid grid-cols-5 gap-4 hover:gap-6 my-4"
                key={index}
              >
                <div className="p-2 rounded-lg shadow-lg bg-rose-400">
                  {item.firstname}
                </div>
                <div className="p-2 rounded-lg shadow-lg bg-slate-500">
                  {item.lastname}
                </div>
                <div className="p-2 rounded-lg shadow-lg bg-sky-700">
                  {item.username}
                </div>
                <div className="p-2 rounded-lg shadow-lg bg-fuchsia-500">
                  {item.age}
                </div>
                <div className="flex flex-row p-2 rounded-lg shadow-lg bg-green-300">
                  <div className="flex flex-row text-sm justify-between w-full">
                    {allRecords.map((record, indexRecord) => {
                      return (
                        <div
                          className="flex flex-col items-center"
                          key={indexRecord}
                        >
                          <div className="flex px-1">{record.title}</div>
                          <div className="flex">{record.score}</div>
                        </div>
                      );
                    })}
                  </div>
                </div>
              </div>
            );
          })}
      </div>
    </>
  );
}
