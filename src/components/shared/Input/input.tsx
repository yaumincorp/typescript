import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";

type InputProps = {
  id: string;
  icon: any;
  onChange: any;
  onBlur: any;
  value?: any;
  type: string;
  name: string;
  placeholder: string;
  dataTestid: string;
  isLastIcon?: boolean;
  lastIconFunction?: (param: boolean) => void;
  paramLastIconFunction?: boolean;
};

const Input = ({
  id,
  type,
  name,
  placeholder,
  icon,
  onChange,
  onBlur,
  value,
  dataTestid,
  isLastIcon,
  lastIconFunction,
  paramLastIconFunction,
}: InputProps) => {
  return (
    <div className="flex flex-row my-4 items-center p-1 border-b-2">
      {icon}

      <input
        type={type}
        id={id}
        name={name}
        placeholder={placeholder}
        data-testid={dataTestid}
        className="input-login"
        value={value}
        onChange={onChange}
        onBlur={onBlur}
      ></input>

      {isLastIcon &&
        (paramLastIconFunction ? (
          <VisibilityIcon
            onClick={() => {
              lastIconFunction(paramLastIconFunction);
            }}
          />
        ) : (
          <VisibilityOffIcon
            onClick={() => {
              lastIconFunction(paramLastIconFunction);
            }}
          />
        ))}
    </div>
  );
};

export default Input;
