import React, { Children } from "react";

type HeaderContext = {
  firstname: string;
  username: string;
};

export const Context = React.createContext<HeaderContext>({
  firstname: "",
  username: "",
});

/* const HeaderContextProvider = (props) => {
  return <Context.Provider value={data}>{props.children}</Context.Provider>;
};

export default HeaderContextProvider; */
