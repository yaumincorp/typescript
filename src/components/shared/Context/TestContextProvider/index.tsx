import React, { Children } from "react";

type TestContextType = {
  name: string;
};

export const TestContext = React.createContext<TestContextType>({
  name: "",
});
