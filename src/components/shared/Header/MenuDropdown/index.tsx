import { useState } from "react";
import { NavLink } from "react-router-dom";

const MenuDropdown = ({ items }: any) => {
  const [dropdown, setDropdown] = useState(false);

  const toggleDropdown = () => {
    setDropdown(!dropdown);
  };

  return (
    <>
      <button className="text-white" onClick={toggleDropdown}>
        {items.title}
      </button>

      <div
        className={`${
          dropdown ? "show menu-dropdown bg-slate-800" : "hide"
        } mx-2`}
      >
        {items?.subMenu.map((menu: any, index: number) => (
          <NavLink
            key={index}
            className={(navData) =>
              navData.isActive ? "nav-link mx-2" : "nav-nolink mx-2"
            }
            to={`/${menu.link}`}
          >
            {menu.title}
          </NavLink>
        ))}
      </div>
    </>
  );
};

export default MenuDropdown;
