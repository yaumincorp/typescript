import { useContext, useState } from "react";
import { Link, NavLink } from "react-router-dom";
import { Context } from "../../Context/HeaderContextProvider";
import Logout from "../Logout";

import HomeIcon from "@mui/icons-material/Home";
import LoginIcon from "@mui/icons-material/Login";
import LogoutIcon from "@mui/icons-material/Logout";
import PersonAddIcon from "@mui/icons-material/PersonAdd";
import SportsEsportsIcon from "@mui/icons-material/SportsEsports";
import BuildIcon from "@mui/icons-material/Build";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import MenuDropdown from "../MenuDropdown";

type HeaderLinksProps = {
  token: string;
  clearToken: () => void;
  isLowDef?: boolean;
};

const HeaderLinks = ({ token, clearToken, isLowDef }: HeaderLinksProps) => {
  const userInformations = useContext(Context);

  const [isDisplay, setIsDisplay] = useState(false);

  const menuItems: any = {
    title: "Games",
    subMenu: [
      {
        title: "Hangman",
        link: "hangman",
      },
      {
        title: "Tic-Tac-Toe",
        link: "tic-tac-toe",
      },
    ],
  };

  return (
    <>
      <div className="hidden xs:flex xs:items-center">
        <NavLink
          to="/"
          className={(navData) =>
            navData.isActive ? "nav-link-home mx-2" : "nav-nolink-home mx-2"
          }
        >
          Home
        </NavLink>

        {token && token.length ? (
          <>
            <MenuDropdown items={menuItems} />
            <NavLink
              to="/tools"
              className={(navData) =>
                navData.isActive ? "nav-link mx-2" : "nav-nolink mx-2"
              }
            >
              Tools
            </NavLink>
            <div
              className="block xl:inline font-bold mx-2 py-3 px-1.5"
              onClick={() => setIsDisplay(!isDisplay)}
            >
              <span className="text-white">{userInformations?.username}</span>

              <div
                className={`${
                  isDisplay ? "block menu-encart w-48 rounded-md" : "hidden"
                }`}
              >
                <span className="text-slate-700">
                  {userInformations?.username}
                </span>
                <Logout
                  className="font-medium text-white hover:text-slate-300 mx-2"
                  clearToken={clearToken}
                  icon={""}
                />
              </div>
            </div>
          </>
        ) : (
          <>
            <NavLink
              to="/login"
              className={(navData) =>
                navData.isActive ? "nav-link mx-2" : "nav-nolink mx-2"
              }
            >
              Log in
            </NavLink>
            <NavLink
              to="/signup"
              className={(navData) =>
                navData.isActive ? "nav-link mx-2" : "nav-nolink mx-2"
              }
            >
              Signup
            </NavLink>
          </>
        )}
      </div>
      <div className="xs:hidden flex items-center">
        <NavLink
          to="/"
          className={(navData) =>
            navData.isActive ? "nav-link-home mx-1" : "nav-nolink-home mx-1"
          }
        >
          <HomeIcon />
        </NavLink>

        {token && token.length ? (
          <>
            <NavLink
              className={(navData) =>
                navData.isActive ? "nav-link mx-1" : "nav-nolink mx-1"
              }
              to="/hangman"
            >
              <SportsEsportsIcon />
            </NavLink>

            <NavLink
              to="/tools"
              className={(navData) =>
                navData.isActive ? "nav-link mx-1" : "nav-nolink mx-1"
              }
            >
              <BuildIcon />
            </NavLink>

            <div
              className="block xl:inline px-1 font-bold mx-1 "
              onClick={() => setIsDisplay(!isDisplay)}
            >
              <AccountCircleIcon className="text-slate-600 hover:text-slate-800" />
              <div
                className={`${
                  isDisplay ? "block menu-encart w-48 rounded-md" : "hidden"
                }`}
              >
                <span className="text-slate-700">
                  {userInformations?.username}
                </span>
                <Logout
                  className="font-medium text-white hover:text-slate-300 mx-1"
                  clearToken={clearToken}
                  icon={<LogoutIcon />}
                />
              </div>
            </div>
          </>
        ) : (
          <>
            <NavLink
              to="/login"
              className={(navData) =>
                navData.isActive ? "nav-link mx-1" : "nav-nolink mx-1"
              }
            >
              <LoginIcon />
            </NavLink>
            <NavLink
              to="/signup"
              className={(navData) =>
                navData.isActive ? "nav-link mx-1" : "nav-nolink mx-1"
              }
            >
              <PersonAddIcon />
            </NavLink>
          </>
        )}
      </div>
    </>
  );
};

export default HeaderLinks;
