import React, { useState, useEffect, createContext, useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import logo from "../../../logo.svg";
import { Context } from "../Context/HeaderContextProvider";
import HeaderLinks from "./HeaderLinks";
import Logout from "./Logout";

type HeaderProps = {
  token: string;
  clearToken: () => void;
  headerText: string;
  otherHeaderText?: string;
  extraText: string;
};

const Header = ({
  token,
  clearToken,
  headerText,
  otherHeaderText = "IDK",
  extraText,
}: HeaderProps) => {
  const userInformations = useContext(Context);

  return (
    <div className="">
      <nav
        className="relative px-2 bg-gray-800 rounded-tl-2xl rounded-tr-2xl flex items-center justify-between sm:h-10 min-h-header lg:justify-start"
        aria-label="Global"
      >
        <div className="flex items-center flex-grow flex-shrink-0 lg:flex-grow-0">
          <div className="flex items-center justify-between w-full md:w-auto">
            <a
              href="#"
              className="flex flex-row-reverse items-center text-white"
            >
              <span>React TS</span>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5 fill-blue-500"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path d="M10 3.5a1.5 1.5 0 013 0V4a1 1 0 001 1h3a1 1 0 011 1v3a1 1 0 01-1 1h-.5a1.5 1.5 0 000 3h.5a1 1 0 011 1v3a1 1 0 01-1 1h-3a1 1 0 01-1-1v-.5a1.5 1.5 0 00-3 0v.5a1 1 0 01-1 1H6a1 1 0 01-1-1v-3a1 1 0 00-1-1h-.5a1.5 1.5 0 010-3H4a1 1 0 001-1V6a1 1 0 011-1h3a1 1 0 001-1v-.5z" />
              </svg>
            </a>
          </div>
        </div>
        <div className="pr-5 md:flex md:ml-10 lg:pr-8 lg:ml-12 md:pr-5 md:space-x-3 lg:space-x-5 items-center">
          <HeaderLinks token={token} clearToken={clearToken} />
        </div>
      </nav>
      <main className="mt-8 mx-auto max-w-7xl px-4 sm:px-6 md:mt-10 lg:mt-4 lg:px-8 xl:mt-12">
        <div className="sm:text-center lg:text-left">
          <h1 className="text-3xl tracking-tight font-extrabold text-gray-900 sm:text-3xl md:text-5xl">
            <span className="block xl:inline px-1">{headerText} </span>
          </h1>

          {"\u00A0"}
          <span className="block text-indigo-600 xl:inline text-radient">
            {otherHeaderText}
          </span>
          {extraText && (
            <p className="mt-1 text-base text-gray-500 sm:mt-2 sm:text-lg sm:max-w-xl sm:mx-auto md:mt-2 md:text-xl lg:mx-0">
              extraText
            </p>
          )}
        </div>
      </main>
    </div>
  );
};

export default Header;
