import { Link, NavLink, useNavigate } from "react-router-dom";

type LogoutProps = {
  clearToken: () => void;
  icon?: any;
  className: string;
};

const Logout = ({ className, clearToken, icon }: LogoutProps) => {
  return (
    <NavLink
      onClick={() => {
        clearToken();
      }}
      to="/"
      className={className}
    >
      {icon ? icon : "Logout"}
    </NavLink>
  );
};

export default Logout;
