import React from "react";
import ReactDOM from "react-dom";

type ModalProps = {
  isShowing: any;
  hide: any;
  titleHeader: string;
  bodyText: string;
  isFooterButtons: boolean;
  firstFunction: () => void;
};

const Modal = ({
  isShowing,
  hide,
  titleHeader,
  bodyText,
  isFooterButtons,
  firstFunction,
}: ModalProps) =>
  isShowing
    ? ReactDOM.createPortal(
        <>
          <div className="modal-overlay">
            <div className="modal-wrapper">
              <div className="modal">
                <div className="modal-header py-4">
                  <h4>{titleHeader}</h4>
                  <button
                    type="button"
                    className="modal-close-button"
                    onClick={hide}
                  >
                    <span>&times;</span>
                  </button>
                </div>
                <div className="modal-body">{bodyText}</div>
                {isFooterButtons && (
                  <div className="modal-footer py-4">
                    <button onClick={firstFunction}>OK</button>
                    <button onClick={hide}>Cancel</button>
                  </div>
                )}
              </div>
            </div>
          </div>
        </>,
        document.body
      )
    : null;

export default Modal;
