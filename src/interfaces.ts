export interface Address {
  street: string;
  number: number;
  zip: string;
}

/* export interface User {
  firstName: string;
  lastName: string;
  pseudo: string;
  address: Address;
  age: number;
  admin: boolean;
}

*/

type Score = {
  score: number;
  difficulty: string;
  _id: string;
  userId: string;
};

export type User = {
  firstname: string;
  lastname: string;
  username: string;
  //address: Address;
  age: number;
  score: Array<Score>;
  //admin: boolean;
};

export type AppProps = {
  routeName: string;
  tokenUserId?: string;
};
