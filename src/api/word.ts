// /api/word.ts

export const getDictionary = async () => {
  return fetch(`${process.env.REACT_APP_API_URL}/hangman`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  }).then((data) => {
    return data.json();
  });
};
