// /api/user.ts

/*
  fetch(`${process.env.REACT_APP_API_URL}/users?page=` + page).then((res) =>
  res.json()
  );
*/

export const getUsers = async (limit: number, skip: number) => {
  console.log("LIMIT", limit);
  console.log("SKIP", skip);
  const response = await fetch(
    `${process.env.REACT_APP_API_URL}/users?limit=${limit}&skip=${skip}`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  if (!response.ok) {
    throw new Error("Network response was not ok");
  }
  return response.json();
};

export const getUser = async (userId: string) => {
  console.log("USER ID", userId);
  return fetch(`${process.env.REACT_APP_API_URL}/users/user/${userId}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  }).then((data) => {
    return data.json();
  });
};

export const checkUser = async (credentials: any) => {
  let data;
  let response = await fetch(
    `${process.env.REACT_APP_API_URL}/users/check?username=${credentials.userName}&password=${credentials.password}`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }
  );

  if (response.status === 200) {
    data = await response.json();
    // handle data
  }
  return data;
};

export const createUser = async (credentials: any) => {
  console.log(credentials);
  return fetch(`${process.env.REACT_APP_API_URL}/users/user/create`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(credentials),
  }).then((data) => data.json());
};

export const loginUser = async (credentials: any) => {
  return fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(credentials),
  }).then((data) => data.json());
};
