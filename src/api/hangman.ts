// /api/hangman.ts

export const createScore = async (
  userId: string,
  difficulty: "easy" | "medium" | "hard" | null,
  score: number
) => {
  return fetch(`${process.env.REACT_APP_API_URL}/users/hangman/createScore`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ userId, difficulty, score }),
  }).then((data) => {
    return data.json();
  });
};

export const updateScore = async (
  scoreId: string,
  difficulty: "easy" | "medium" | "hard" | null,
  score: number
) => {
  console.log("JE SUIS RENTRE DANS LUPDATE");
  return fetch(`${process.env.REACT_APP_API_URL}/users/hangman/updateScore`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ scoreId, difficulty, score }),
  }).then((data) => {
    return data.json();
  });
};
