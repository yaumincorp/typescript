import React, { useState } from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import "./App.css";

//shared
import Header from "./components/shared/Header";

//pages
import HomePage from "./components/pages/HomePage";
import HangmanPage from "./components/pages/HangmanPage";
import LoginPage from "./components/pages/LoginPage";
import SignupPage from "./components/pages/SignupPage";
import ToolsPage from "./components/pages/ToolsPage";
import TicTacToePage from "./components/pages/TicTacToePage";

//stuff
import useToken from "./hooks/token/useToken";
import { useUser } from "./hooks/query/user/useUser";
import { Context } from "./components/shared/Context/HeaderContextProvider";
import { TestContext } from "./components/shared/Context/TestContextProvider";
import TestPage from "./components/pages/TestPage";
import NoMatch from "./components/pages/NoMatchPage";

function App() {
  const { token, setToken, clearToken } = useToken();
  console.log("TOKEN", token);
  console.log("TOKEN LENGTH", token.token.length);
  const { data, isLoading, isError } = useUser(token?.userId);

  {
    /* Refacto this page to add routes in a container */
  }

  return (
    <div className="App bg-gray-100 h-full">
      <Context.Provider value={data}>
        <Header
          token={token.token}
          clearToken={clearToken}
          headerText="Title header"
          extraText="Si vous vous ennuyez quand vous traînez sur l'ordi ou même lors d'un dimanche pluvieux, trouvez une activité"
        />
      </Context.Provider>
      <Routes>
        <Route path="/" element={<HomePage routeName="Home" />}></Route>

        <Route
          path="/test"
          element={
            <TestContext.Provider value={{ name: "Toto" }}>
              <TestPage />
            </TestContext.Provider>
          }
        ></Route>

        <Route
          path="/login"
          element={<LoginPage routeName="Login" setToken={setToken} />}
        ></Route>
        <Route
          path="/signup"
          element={<SignupPage routeName="SignUp" />}
        ></Route>
        {token && (
          <Route
            path="/hangman"
            element={
              token.token.length ? (
                <HangmanPage routeName="Hangman" tokenUserId={token.userId} />
              ) : (
                <Navigate to="/" />
              )
            }
          >
            <Route
              path=":userId/:difficulty"
              element={
                token.token.length ? (
                  <HangmanPage routeName="Hangman" tokenUserId={token.userId} />
                ) : (
                  <Navigate to="/" />
                )
              }
            ></Route>
          </Route>
        )}
        {token && (
          <Route
            path="tic-tac-toe"
            element={<TicTacToePage routeName="Tic Tac Toe" />}
          ></Route>
        )}
        {token && (
          <Route
            path="/tools"
            element={<ToolsPage routeName="Tools" />}
          ></Route>
        )}
        <Route path="*" element={<NoMatch />} />
      </Routes>
    </div>
  );
}

export default App;
