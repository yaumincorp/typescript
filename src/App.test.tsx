import { render, screen } from "@testing-library/react";
import App from "./App";
import { BrowserRouter, Router } from "react-router-dom";
import userEvent from "@testing-library/user-event";
import { createMemoryHistory } from "history";
import { QueryClient, QueryClientProvider } from "react-query";

describe("Renders App JS in many cases", () => {
  it("renders learn react title", () => {
    const queryClient = new QueryClient();

    render(
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </QueryClientProvider>
    );
    const titleElement = screen.getByText(/React TS/i);
    expect(titleElement).toBeInTheDocument();
  });

  it("renders prompt to user", () => {
    const queryClient = new QueryClient();
    render(
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </QueryClientProvider>
    );
    const pElement = screen.getByText((_content, node) =>
      /^Next$/i.test(node.textContent)
    );
    expect(pElement).toBeInTheDocument();
  });

  it("full app rendering/navigating", async () => {
    const history = createMemoryHistory();
    const queryClient = new QueryClient();
    render(
      <Router location={history.location} navigator={history}>
        <QueryClientProvider client={queryClient}>
          <App />
        </QueryClientProvider>
      </Router>
    );
    //const user = userEvent.setup();
    // verify page content for expected route
    // often you'd use a data-testid or role query, but this is also possible
    expect(screen.getByTestId("home-page-name")).toHaveTextContent("Home");

    //await user.click(screen.getByText(/about/i))

    // check that the content changed to the new page
    //expect(screen.getByText(/you are on the about page/i)).toBeInTheDocument()
  });
});
