const ticTacToeHelper = (
  playerBoxes: any,
  iaBoxes: any,
  availableBoxes: any
) => {
  const victoryCases = [
    ["tl", "tc", "tr"],
    ["ml", "mc", "mr"],
    ["bl", "bc", "br"],
    ["tl", "mc", "br"],
    ["bl", "mc", "tr"],
    ["tl", "ml", "bl"],
    ["tc", "mc", "bc"],
    ["tr", "mr", "br"],
  ];

  //Create ia to improve difficulty
  //console.log("availableBoxes for ia play", availableBoxes);
  //console.log("IA BOXES", iaBoxes);
  //console.log("PLAYER BOXES", playerBoxes);

  //Ia is checking player boxes
  //const comparePlayerBoxesWithVictory =

  //Ia is checking player boxes
  let playerVictoryBoxes: any[] = [];
  let iaVictoryBoxes: any[] = [];

  //Define if player play in first and in the mc box, ia needs to put a box
  //in tl, tr, ml, mr

  victoryCases.forEach((item) => {
    const intersection = item.filter((element) =>
      playerBoxes.includes(element)
    );
    playerVictoryBoxes.push({ inGamePlay: intersection, victory: item });

    const intersectionIa = item.filter((element) => iaBoxes.includes(element));
    iaVictoryBoxes.push({ inGamePlay: intersectionIa, victory: item });
  });

  const mostVictoryChancePlayer: any[] = playerVictoryBoxes.filter(
    (element) => element.inGamePlay.length >= 2
  );
  const mostVictoryChanceIa: any[] = iaVictoryBoxes.filter(
    (element) => element.inGamePlay.length >= 1
  );

  let victoryCase: any;
  let iaVictoryCase: any;

  mostVictoryChancePlayer.forEach((victoryChance) => {
    victoryCase = victoryChance.victory
      .filter((filtered: any) => !victoryChance.inGamePlay.includes(filtered))
      .join("");
  });

  mostVictoryChanceIa.forEach((victoryChance) => {
    iaVictoryCase = victoryChance.victory.filter(
      (filtered: any) => !victoryChance.inGamePlay.includes(filtered)
    );
  });

  const counterBox = availableBoxes.find(
    (availableBox: any) => availableBox.box === victoryCase
  );
  console.log("availableBoxes", availableBoxes);
  const winnableBox = availableBoxes.find((availableBox: any) => {
    console.log("IA VICTORY CASE", iaVictoryCase);
    console.log("availableBox", availableBox);

    if (iaVictoryCase) {
      return iaVictoryCase[Math.floor(Math.random() * iaVictoryCase.length)];
    } else {
      return availableBoxes[Math.floor(Math.random() * availableBoxes.length)];
    }
  });

  console.log("counterBox", counterBox);
  console.log("winnableBox", winnableBox);
};

export default ticTacToeHelper;
