export const validateInput = (name: string, value: string) => {
  let hasError = false;
  let error = "";
  console.log("age", Number(value));
  switch (name) {
    case "firstname":
      console.log("yo lesq kheys");
      if (!value.length) {
        hasError = true;
        error = "Firstname cannot be empty";
      } else if (!/^[a-zA-Z ]+$/.test(value)) {
        hasError = true;
        error = "Invalid firstname. Avoid Special characters";
      } else {
        hasError = false;
        error = "";
      }
      break;
    case "lastname":
      if (value.trim() === "") {
        hasError = true;
        error = "Lastname cannot be empty";
      } else if (!/^[a-zA-Z ]+$/.test(value)) {
        hasError = true;
        error = "Invalid lastname. Avoid Special characters";
      } else {
        hasError = false;
        error = "";
      }
      break;
    case "username":
      if (value.trim() === "") {
        hasError = true;
        error = "Username cannot be empty";
      } else if (!/^[a-zA-Z ]+$/.test(value)) {
        hasError = true;
        error = "Invalid username. Avoid Special characters";
      } else {
        hasError = false;
        error = "";
      }
      break;
    case "age":
      if (Number(value) < 0 || Number(value) > 200) {
        console.log("COUCOU");
        hasError = true;
        error = "Age needs to be between 0 et 200";
      } else {
        console.log("COUCOU LA ", value);
        hasError = false;
        error = "";
      }
      break;
    case "password":
      if (value.trim() === "") {
        hasError = true;
        error = "Password cannot be empty";
      } else if (!/^[a-zA-Z ]+$/.test(value)) {
        hasError = true;
        error = "Invalid password.";
      } else {
        hasError = false;
        error = "";
      }
      break;
    default:
      break;
  }

  return { hasError, error };
};
