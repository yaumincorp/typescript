/*
        Dans cet exercice, j'aimerai que vous me créiez une fonction somme qui calculera la somme des nombres 
        en partant de 1 jusqu'au nombre que nous lui donnerons en paramètres.
        Par exemple, si je donne 5 en paramètres, alors ma fonction devra me calculer 5 + 4 + 3 + 2 + 1. 
        Notre fonction devra donc nous retourner 15.
        Si vous réussissez cet exercice, vous pourrez être sûr d'avoir compris le principe des fonctions 
        récursives ! N'hésitez pas à vous aider de la session précédente, dans laquelle nous créons notre timer, 
        ceci vous aidera beaucoup !
    */

export const sum = (numberToSum: number) => {
  let nb: number = numberToSum;
  if (numberToSum > 0) {
    nb += sum(numberToSum - 1);
  }
  return nb;
};

const total = sum(5);

console.log("TOTAL", total);
